#!/bin/bash
lv_array=(`lvs | grep osd-block | awk '{print "/dev/"$2"/"$1}'`)
for lv in ${lv_array[@]};do
#echo ${lv}
lvremove ${lv} --yes
done
vg_array=(`vgs | grep ceph | awk '{print $1}'`)
for vg in ${vg_array[@]};do
#echo ${vg}
vgremove ${vg} --yes
done
pv_array=(`pvs | grep lvm | awk '{print $1}'`)
for pv in ${pv_array[@]};do
echo ${pv}
pvremove ${pv}
done

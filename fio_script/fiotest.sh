nfs=$1
tcp_udp=$2
ip=$3
bs=$4
size=$5
rw=$6
rwmixread=""
if [ -n "$7" ] ;then
    rwmixread="-rwmixread=$7"
fi
current_path=$(cd "$(dirname $0)";pwd)
cd $current_path
echo "nfs=$1,tcp_udp=$2,ip=$3,bs=$4,size=$5,rw=$6,rwmixread=$rwmixread"
./mount.sh u
expect <<EOF
set timeout 30
spawn ssh root@$ip
expect {
    "*yes/no" { send "yes\r" }
    "*password:" { send "37-Ehu@lu\r" }
}
expect -re ".*\[\$#\]"
    send "/jtest/fio_script/restartGs.sh 1\r"
    expect -re ".*\[\$#\]"
    send "exit\r"
    expect eof
#interact
expect eof
EOF

./mount.sh m $nfs $ip $tcp_udp ||exit
cd /mnt/test/bkt1 ||exit
ls
ps -ef |grep 'fio  -direct=1'
sleep 20
fiocmd="fio  -direct=1 -iodepth 1 -thread -rw=$rw $rwmixread -ioengine=libaio -bs=$bs -size=$size -numjobs=30 -group_reporting -name=mytest"
echo "执行测试："$fiocmd
starttime=`date +'%Y-%m-%d %H:%M:%S'`
echo "开始时间："$starttime
$fiocmd 
endtime=`date +'%Y-%m-%d %H:%M:%S'`
start_seconds=$(date --date="$starttime" +%s);
end_seconds=$(date --date="$endtime" +%s);
echo "结束时间："$endtime
echo "本次fio测试时间： "$(((end_seconds-start_seconds)/60))"分"$(((end_seconds-start_seconds)%60))"秒"
cd /

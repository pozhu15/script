#!/bin/bash

ip=$1
cmd=""

total=$#
array=($*)
for ((i=1;i<=$total;i++));
do
    if [ $i -gt 1 ] ;then
         cmd=$cmd${array[$i-1]}" "
    fi
done

echo $cmd 

expect <<EOF
set timeout 30
spawn ssh root@$ip
expect {
    "*yes/no" { send "yes\r" }
    "*password:" { send "37-Ehu@lu\r" }
}
expect -re ".*\[\$#\]"
    send "$cmd\r"
    expect -re ".*\[\$#\]"
    send "exit\r"
    expect eof
#interact
expect eof
EOF

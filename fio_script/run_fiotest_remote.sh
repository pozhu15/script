current_path=$(cd "$(dirname $0)";pwd)
cd $current_path

umount_fun(){ #取消节点挂载
    for var in $@
	do
	./run_on_remote2.sh 172.38.40.19$var /jtest/fio_script/mount.sh u 
    done
}

mount_fun(){ #挂载,参数分别为：需要挂载的客户端，挂载点，挂载协议，tcp/udp
    ./run_on_remote2.sh 172.38.40.19$1 /jtest/fio_script/mount.sh m $3 172.38.40.19$2 $4
} 
restart_ganesha(){	#重启节点1服务
    for var in $@
        do
	./run_on_remote2.sh 172.38.40.19$var /jtest/fio_script/restartGs.sh 1
    done
}

fio_cmd(){ #fio 参数：节点尾数 , bs , size, rw, rwmixread
# 3  1M 5G write

node_bkt=("bkt1" "bkt2" "bkt3" "bkt4")

if [ ! -d "/mnt/test/${node_bkt[$1-3]}" ];then
  mkdir "/mnt/test/${node_bkt[$1-3]}"
fi
bs=$2
size=$3
rw=$4
rwmixread=""
if [ -n "$5" ] ;then
    rwmixread="-rwmixread=$5"
fi

fiocmd="cd /mnt/test/${node_bkt[$1-3]} ||exit ; ls ; fio  -direct=1 -iodepth 1 -thread -rw=$rw $rwmixread -ioengine=libaio -bs=$bs -size=$size -numjobs=30 -group_reporting -name=mytest"
echo "执行测试："$fiocmd
starttime=`date +'%Y-%m-%d %H:%M:%S'`
echo "开始时间："$starttime
./run_on_remote2.sh 172.38.40.19$1 $fiocmd >>result/node$1.log
endtime=`date +'%Y-%m-%d %H:%M:%S'`
start_seconds=$(date --date="$starttime" +%s);
end_seconds=$(date --date="$endtime" +%s);
echo "结束时间："$endtime
echo "本次fio测试时间： "$(((end_seconds-start_seconds)/60))"分"$(((end_seconds-start_seconds)%60))"秒"

}

remove_cmd(){ #删除桶数据,参数：节点
node_bkt=("bkt1" "bkt2" "bkt3" "bkt4")
rm_cmd="rm -rf  /mnt/test/${node_bkt[$1-3]}/*"
./run_on_remote2.sh 172.38.40.19$1 $rm_cmd
}

umount_fun 3 4 5 6   #取消193-196节点的挂载

restart_ganesha 3    #重启193ganesha

mount_fun 4 3 4.0 tcp #194节点挂载193,4.0，tcp协议
mount_fun 5 3 4.0 tcp #195节点挂载193,4.0，tcp协议
fio_cmd 5  1M 5G write &
fio_cmd 4  1M 5G write &
wait
echo read
fio_cmd 4  1M 5G read &
fio_cmd 5  1M 5G read &
wait
echo 开始删除
remove_cmd 4

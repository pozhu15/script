if [ $1 == 'u' ] ;then
    umount -l `mount |grep mnt/test |awk '{print($3)}'`
    echo '取消挂载状态：'
    nfsstat -m
elif [ $1 == 'm' ] ; then
        mountCmd="mount -t nfs"
        if [ ! -n "$2" ] || [ ! -n "$3" ];then
                echo "参数不全！"
                exit
        fi
        case $2 in
        3)  #echo 'v3'
                mountCmd=$mountCmd" -o vers=3,sync"
                if [ -n "$4" ] && [ $4 == "tcp" ] ;then
                        mountCmd=$mountCmd",tcp "
                else
                        mountCmd=$mountCmd" "
                fi
        ;;
        4.0)  #echo 'v4.0'
                mountCmd=$mountCmd"4 -o vers=4.0,sync "
        ;;
        4.1)  #echo 'v4.1'
                mountCmd=$mountCmd"4 -o vers=4.1,sync "
        ;;
        4.2)  #echo 'v4.2'
                mountCmd=$mountCmd"4 -o vers=4.2,sync "
        ;;
        *)  echo "协议参数有误！"
        ;;
   esac
   mountCmd=$mountCmd"$3:/ /mnt/test"
   echo "开始挂载：$mountCmd"
   $mountCmd
   echo "挂载状态："
   nfsstat -m
else
   echo "参数说明示例"    
	echo './mount.sh u  #取消所有/mnt/test的挂载'
	echo './mount.sh m 3 172.38.40.193 tcp  	#挂载NFSv3 tcp ,172.38.40.193挂载到/mnt/test'
	echo './mount.sh m 3 172.38.40.193   		#挂载NFSv3 udp ,172.38.40.193挂载到/mnt/test'
	echo './mount.sh m 4.0 172.38.40.193  	#挂载NFSv4.0 ,172.38.40.193挂载到/mnt/test'  
fi

#arg1 ：nfs
#arg2 ：tcp_udp
#arg3 ：ip
#arg4 ：bs
#arg5 ：size
#arg6 ：rw
#arg7 ：rwmixread

ip=172.38.40.193

current_path=$(cd "$(dirname $0)";pwd)
cd $current_path

clear_bkt(){
    while :
    do
	rm -rf /mnt/test/bkt1/*
    	cstr=`ls /mnt/test/bkt1/`
	if [ ! -n "$cstr" ] ;then
	    break
	fi
	sleep 1
    done
    sleep 10
}
test_write_read_1M(){	#测试顺序读写1M
clear_bkt
./fiotest.sh 3 tcp 172.38.40.193 1M 5G write
./fiotest.sh 3 tcp 172.38.40.193 4M 5G read
clear_bkt
./fiotest.sh 3 udp 172.38.40.193 1M 5G write
./fiotest.sh 3 udp 172.38.40.193 4M 5G read
clear_bkt
./fiotest.sh 4.0 tcp 172.38.40.193 1M 5G write
./fiotest.sh 4.0 tcp 172.38.40.193 4M 5G read
clear_bkt
./fiotest.sh 4.1 tcp 172.38.40.193 1M 5G write
./fiotest.sh 4.1 tcp 172.38.40.193 4M 5G read
clear_bkt
./fiotest.sh 4.2 tcp 172.38.40.193 1M 5G write
./fiotest.sh 4.2 tcp 172.38.40.193 4M 5G read
}
test_write_read_4k(){	#测试顺序读写4k
clear_bkt
./fiotest.sh 3 tcp 172.38.40.193 4k 100M write
./fiotest.sh 3 tcp 172.38.40.193 4k 100M read
clear_bkt
./fiotest.sh 3 udp 172.38.40.193 4k 100M write
./fiotest.sh 3 udp 172.38.40.193 4k 100M read
clear_bkt
./fiotest.sh 4.0 tcp 172.38.40.193 4k 100M write
./fiotest.sh 4.0 tcp 172.38.40.193 4k 100M read
clear_bkt
./fiotest.sh 4.1 tcp 172.38.40.193 4k 300M write
./fiotest.sh 4.1 tcp 172.38.40.193 4k 300M read
clear_bkt
./fiotest.sh 4.2 tcp 172.38.40.193 4k 300M write
./fiotest.sh 4.2 tcp 172.38.40.193 4k 300M read

}

testRW_1M(){	#测试混合读写1M
clear_bkt
./fiotest.sh 3 tcp 172.38.40.193 1M 1G rw 30
clear_bkt
./fiotest.sh 3 udp 172.38.40.193 1M 1G rw 30
clear_bkt
./fiotest.sh 4.0 tcp 172.38.40.193 1M 1G rw 30
clear_bkt
./fiotest.sh 4.1 tcp 172.38.40.193 1M 1G rw 30
clear_bkt
./fiotest.sh 4.2 tcp 172.38.40.193 1M 1G rw 30

clear_bkt
./fiotest.sh 3 tcp 172.38.40.193 1M 1G rw 50
clear_bkt
./fiotest.sh 3 udp 172.38.40.193 1M 1G rw 50
clear_bkt
./fiotest.sh 4.0 tcp 172.38.40.193 1M 1G rw 50
clear_bkt
./fiotest.sh 4.1 tcp 172.38.40.193 1M 1G rw 50
clear_bkt
./fiotest.sh 4.2 tcp 172.38.40.193 1M 1G rw 50

clear_bkt
./fiotest.sh 3 tcp 172.38.40.193 1M 1G rw 70
clear_bkt
./fiotest.sh 3 udp 172.38.40.193 1M 1G rw 70
clear_bkt
./fiotest.sh 4.0 tcp 172.38.40.193 1M 1G rw 70
clear_bkt
./fiotest.sh 4.1 tcp 172.38.40.193 1M 1G rw 70
clear_bkt
./fiotest.sh 4.2 tcp 172.38.40.193 1M 1G rw 70
}

testRW_4k(){	#测试混合读写4k
$rmcmd
./fiotest.sh 3 tcp 172.38.40.193 4k 100M rw 30
$rmcmd
./fiotest.sh 3 udp 172.38.40.193 4k 100M rw 30
$rmcmd
./fiotest.sh 4.0 tcp 172.38.40.193 4k 100M rw 30
$rmcmd
./fiotest.sh 4.1 tcp 172.38.40.193 4k 100M rw 30
$rmcmd
./fiotest.sh 4.2 tcp 172.38.40.193 4k 100M rw 30

$rmcmd
./fiotest.sh 3 tcp 172.38.40.193 4k 100M rw 50  
$rmcmd
./fiotest.sh 3 udp 172.38.40.193 4k 100M rw 50  
$rmcmd
./fiotest.sh 4.0 tcp 172.38.40.193 4k 100M rw 50  
$rmcmd
./fiotest.sh 4.1 tcp 172.38.40.193 4k 100M rw 50
$rmcmd
./fiotest.sh 4.2 tcp 172.38.40.193 4k 100M rw 50

$rmcmd
./fiotest.sh 3 tcp 172.38.40.193 4k 100M rw 70    
$rmcmd
./fiotest.sh 3 udp 172.38.40.193 4k 100M rw 70 
$rmcmd
./fiotest.sh 4.0 tcp 172.38.40.193 4k 100M rw 70  
$rmcmd
./fiotest.sh 4.1 tcp 172.38.40.193 4k 100M rw 70
$rmcmd
./fiotest.sh 4.2 tcp 172.38.40.193 4k 100M rw 70
}


test_write_read_1M >/jtest/fio_script/result/test_write_read_1M.log	#测试顺序读写1M

test_write_read_4k >/jtest/fio_script/result/test_write_read_4k.log	#测试顺序读写4k

testRW_4k >/jtest/fio_script/result/testRW_4k.log  	#测试混合读写4k

testRW_1M >/jtest/fio_script/result/testRW_1M.log	#测试混合读写1M

ip=$1
expect <<EOF
set timeout 30
spawn scp -r /jtest/fio_script $ip:/jtest/
expect {
    "*yes/no" { send "yes\r" }
    "*password:" { send "37-Ehu@lu\r" }
}
expect -re ".*\[\$#\]"
send "exit\r"
#interact
expect eof
EOF

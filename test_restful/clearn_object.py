from rados import (Rados, Error, RadosStateError, Object, ObjectExists,
                   ObjectNotFound, ObjectBusy, requires, opt,
                   LIBRADOS_ALL_NSPACES, WriteOpCtx, ReadOpCtx,
                   LIBRADOS_SNAP_HEAD, LIBRADOS_OPERATION_BALANCE_READS, LIBRADOS_OPERATION_SKIPRWLOCKS, MonitorLog)

rados = Rados(conffile='')
rados.conf_parse_env('FOO_DOES_NOT_EXIST_BLAHBLAH')
rados.conf_parse_env()
rados.connect()
ioctx = rados.open_ioctx('ganesha_data')  
ioctx.set_namespace('ganesha-export-index') 

def show_all_object(): #显示所有对象
    object_names = [obj.key for obj in ioctx.list_objects()]   #
    print('列出所有对象')
    print(object_names)
    for obj in ioctx.list_objects():
        size, _ = obj.stat()
        context = obj.read(size)
        context=context.decode("utf-8")
        print(obj.key+':', context)

def remove_all_object():
    for obj in ioctx.list_objects():
        ioctx.remove_object(obj.key)

def remove_object(startsw):
    objs = ioctx.list_objects()
    for obj in objs:
        if obj.key.startswith(startsw):
            ioctx.remove_object(obj.key)

#show_all_object()

#remove_all_object()

remove_object('rgw_export-')
remove_object('HAG_vip-')
remove_object('domain_names-')
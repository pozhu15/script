#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import requests, json
import os,sys,re,time
from rados import (Rados, Error, RadosStateError, Object, ObjectExists,
                   ObjectNotFound, ObjectBusy, requires, opt,
                   LIBRADOS_ALL_NSPACES, WriteOpCtx, ReadOpCtx,
                   LIBRADOS_SNAP_HEAD, LIBRADOS_OPERATION_BALANCE_READS, LIBRADOS_OPERATION_SKIPRWLOCKS, MonitorLog)

rados = Rados(conffile='')
rados.conf_parse_env('FOO_DOES_NOT_EXIST_BLAHBLAH')
rados.conf_parse_env()
rados.connect()

script_path = os.path.realpath(__file__)
option1=''
if len(sys.argv)>1:
    option1=sys.argv[1]
MY_ADMIN_TOKEN='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJjZXBoLWRhc2hib2FyZCIsImlhdCI6MTYyMTg0NzYxNywidXNlcm5hbWUiOiJhZG1pbiIsImp0aSI6ImJmMmZmZWQ2LWFkMzEtNGRhYS04ZDFlLWNkN2U5YzM2OWQ4YSIsImV4cCI6MTYyMTg3NjQxN30.kU3EXAl4tNoymlTWo6D82m7ZM-Y7b4lAFpgQ2VO85zo'

URL='http://192.168.110.187:8444'

def get_token():    #获取token
    url=URL+'/api/auth'
    headers={'Content-Type':'application/json',
            'Accept': 'application/vnd.ceph.api.v1.0+json',
            }
    data = {"username": "admin", "password": "admin"}
    result = requests.post(
        url,
        #data=json.dumps(data),
        json=data,
        headers=headers,
    )
    return result.json()['token']


def send_request(method,url,data,msg,return_json=True):    #发送post
    print(msg)
    headers={'Content-Type':'application/json',
            'Accept': 'application/vnd.ceph.api.v1.0+json',
            'Authorization':'Bearer ' +MY_ADMIN_TOKEN,
            }
    request_method=None
    if  method=='POST':
        request_method= requests.post
    elif  method=='GET':
        request_method= requests.get
    elif  method=='DELETE':
        request_method= requests.delete
    elif  method=='PUT':
        request_method= requests.put
    result = request_method(
        url,
        data=json.dumps(data),
        #json=data,
        headers=headers,
    )
    if return_json==True:
      return result.json()
    else:
      return {'status':result.status_code,'text':result.text}

if  option1== '-t':     #获取token
    token=get_token()
    content=''
    with open(script_path,'r') as me:
        script=me.read()
        pattern = re.compile(r'MY_ADMIN_TOKEN.+\'(?=\n)') 
        match=pattern.findall(script)
        #print(match)
    content = pattern.sub('MY_ADMIN_TOKEN=\''+token+'\'', script)
    with open(script_path,'w') as me:
        me.write(content)
    os.system('python '+script_path)
    exit()
elif  option1== '-d':   #删除export
    exports=send_request('GET',URL+'/api/nfs-ganesha/export', '', '查看export列表') 
    print ('删除导出   ', exports)
    for ex in exports:
      eid=''+str(ex['export_id'])
      print(eid)
      print(send_request('DELETE',URL+"/api/nfs-ganesha/export/_default_/"+eid+"?reload_daemons=true", '', '删除export :'+eid))
    exit()  
    
    
create_export= {     #创建nfs export
  "clients": [
    {
      "addresses": ["12"],
      "access_type": "RW",
      "squash": "root"
    }
  ],
  "pseudo": "/y",
  "squash": "root",
  "access_type": "RW",
  "security_label": "",
  "cluster_id": "_default_",
  "tag": "gdfjvh",
  "fsal": {
    "rgw_user_id": "admin",
    "sec_label_xattr": "111",
    "user_id": "admin",
    "name": "RGW",
    "filesystem": "RGW",
    "RGW":{
        "name":"client.rgw.",
        "ceph_conf":"/etc/ceph/ceph.conf",
        "cluster":"ceph",
        "init_args":"--log_file=/var/log/ganesha/rgw.log --debug-rgw=1 --rgw_nfs_s3_fast_attrs=true"
    }
  },
  "reload_daemons": True,
  "daemons": [
    "node1"
  ],
  "path": "/",
  "transports": [
    "TCP"
  ],
  "protocols": [
    4
  ],
  "expend": { 
    "expend": { 
    "grace_period": 10,
    "Anonymous_Uid": 0,
    "Anonymous_Gid": 0
  }
  }
}

def test_nfs():
  print(send_request('GET',URL+'/api/nfs-ganesha/daemon', '', '查看daemon列表') )  
  exports=send_request('GET',URL+'/api/nfs-ganesha/export', '', '查看export列表') 
  ioctx = rados.open_ioctx('ganesha_data')
  ioctx.set_namespace('ganesha-export-index')
  objs=ioctx.list_objects()
  keeys=[]
  for obj in objs:
      keeys.append(obj.key)
  print(keeys)
  print(exports) 

  print(send_request('GET',URL+'/api/host/hostsaddr', create_export, '获取集群节点列表'))
  print(send_request('GET',URL+'/api/host/rgwnfs', create_export, '获取rgw-nfs节点列表'))


#os.system('rados -p ganesha_data ls')
#print(rados.pool_exists('ganesha_data'))
ioctx = rados.open_ioctx('ganesha_data')  #打开pool
ioctx.set_namespace('ganesha-export-index') #命名空间
ioctx.list_objects()
raw_config=' EXPORT {\
    grace_period = 0;\
    FSAL {\
        secret_access_key = "admin";\
        user_id = "admin";\
        name = "RGW";\
        access_key_id = "admin";\
    }\
\
    CLIENT {\
        clients = "132.45";\
        squash = "root_squash";\
        access_type = "RW";\
    }\
\
    Anonymous_Gidfsdfdfdfdf = 0;\
    pseudo = "/";\
    squash = "root_squash";\
    access_type = "RW";\
    tag = "gdfj11h";\
    Anonymous_Gid = 0;\
    Anonymous_Uid = 0;\
    path = "/i";\
    export_id = 1;\
    transports = "TCP";\
    protocols = 4;\
}\
\
RGW {\
    ceph_conf = "/etc/ceph/ceph.conf";\
    name = "client.rgw.node1";\
    init_args = "--log_file=/var/log/ganesha/rgw.log --debug-rgw=1 --rgw_nfs_s3_fast_attrs=true";\
    cluster = "ceph";\
}'

change_export= {     #创建nfs export
  "clients": [
    {
      "addresses": [
        "211","55"
      ],
      "access_type": "RW",
      "squash": "root"
    }
  ],
  "pseudo": "/y",
  "squash": "root",
  "access_type": "RW",
  "security_label": "",
  "cluster_id": "_default_",
  "tag": "test_change",
  "fsal": {
    "sec_label_xattr": "111",
    "user_id": "admin",
    "name": "RGW",
    "filesystem": "RGW",
    "RGW":{
        "name":"client.rgw.node1",
        "ceph_conf":"/etc/ceph/ceph.conf",
        "cluster":"ceph",
        "init_args":"--log_file=/var/log/ganesha/rgw.log --debug-rgw=1 --rgw_nfs_s3_fast_attrs=true"
    }
  },
  "reload_daemons": True,
  "daemons": [
    "node1"
  ],
  "path": "/",
  "transports": [
    "TCP"
  ],
  "protocols": [
    4
  ],
  "expend": { 
    "grace_period": 0,
    "Anonymous_Uid": 0,
    "Anonymous_Gid": 0
  }
}

#print(send_request('DELETE',URL+'/api/nfs-ganesha/export/_default_/1?reload_daemons=true', '', '删除export'))
change_clients= {     
  "clients": [
    {
      "addresses": [
        "187"
      ],
      "access_type": "RW",
      "squash": "root"
    },
    {
      "addresses": [
        "110"
      ],
      "access_type": "OR",
      "squash": "root_squash"
    }
  ]
}

def test_nfs():
  print(send_request('POST',URL+'/api/nfs-ganesha/export', create_export, '创建export'))
  print(send_request('GET',URL+'/api/nfs-ganesha/export', '', '查看export列表')) 
  print(send_request('GET',URL+'/api/nfs-ganesha/status', create_export, '查看ganesha状态'))
  print(send_request('PUT',URL+'/api/nfs-ganesha/export/_default_/1', change_export, '修改export'))
  print(send_request('PUT',URL+'/api/nfs-ganesha/export/_default_/2', change_export, '修改export'))
  print(send_request('PUT',URL+'/api/nfs-ganesha/client/_default_/1', change_clients, '修改clients'))
  print(send_request('GET',URL+'/api/nfs-ganesha/client/_default_/1', '', '查看clients'))
  print(send_request('GET',URL+'/ui-api/nfs-ganesha/clusters', '', '查看集群ID'))
  print(send_request('GET',URL+'/ui-api/nfs-ganesha/cephx/clients', '', 'test'))
  print(send_request('GET',URL+'/ui-api/nfs-ganesha/rgw/buckets', '', 'test'))
  #print(send_request('GET',URL+'/ui-api/nfs-ganesha/lsdir', '', 'test'))
  

domain_name={"domain_name":{
  "domain_name":"www.baidu4.com", 
  "ip_list":[
    "192.168.110.187",
    "192.168.110.123"
  ],
  "vip_list":[{
    'name':'vip001',
    'vip_ip':'192.168.110.123',
    'nfs_gateway_ip':[
      "192.168.110.187",
      "192.168.110.123"
    ]
    }
  ]
},
"cluster_id":"_default_"}


vip={
  "domain_name_id": 1,
  "vip": {
    'name':'vip002',
    'vip_ip':'192.168.110.123',
    'nfs_gateway_ip':[
      "192.168.110.187",
      "192.168.110.123"
    ]
    },
  "cluster_id": "_default_"
}

iplist={
  "domain_name_id": 1,
  "cluster_id": "_default_",
  "ip_list": ["192.168.110.187"
      ]
}

domain={
  "domain_name_id": 1,
  "cluster_id": "_default_",
  "domain_name": "www.tengxun.com"
}
def test_domain_name():
  print(send_request('DELETE',URL+'/api/nfs-ganesha/domain_name/_default_/1', '' , '删除域名',return_json=False))
  print(send_request('DELETE',URL+'/api/nfs-ganesha/domain_name/_default_/2', '' , '删除域名',return_json=False))
  print(send_request('DELETE',URL+'/api/nfs-ganesha/domain_name/_default_/3', '' , '删除域名',return_json=False))
  print(send_request('DELETE',URL+'/api/nfs-ganesha/domain_name/_default_/4', '' , '删除域名',return_json=False))
  print(send_request('POST',URL+'/api/nfs-ganesha/domain_name', domain_name , '添加域名'))
  #print(send_request('PUT',URL+'/api/nfs-ganesha/domain_name/_default_/1', domain_name , '修改域名'))

  print(send_request('GET',URL+'/api/nfs-ganesha/domain_name/_default_', '' , '查看域名'))
  print(send_request('PUT',URL+'/api/nfs-ganesha/domain_name/add_vip', vip , '添加高可用组'))
  #print(send_request('DELETE',URL+'/api/nfs-ganesha/domain_name/_default_/1', '' , '删除域名',return_json=False))
  print(send_request('PUT',URL+'/api/nfs-ganesha/domain_name/change_domain_name', domain , '修改域名名称'))
  print(send_request('GET',URL+'/api/nfs-ganesha/domain_name/_default_', '' , '查看域名'))
  print(send_request('DELETE',URL+'/api/nfs-ganesha/domain_name/del_vip/_default_/1/vip001', vip , '删除高可用组'))
  print(send_request('PUT',URL+'/api/nfs-ganesha/domain_name/set_ip_list', iplist , '重新设置域名绑定的IP地址列表'))
  print(send_request('GET',URL+'/api/nfs-ganesha/domain_name/_default_', '' , '查看域名'))

rgw_export={
    "daemon": "192.168.110.187",
    "clients": [
          "192.168.110.18",
      "192.168.110.12"
    ],
    "bucket": "y",
    "tag": "nfs02",
    "cluster_id": "_default_",
    "reload_daemons": True,
    "rgw_user_id": "admin",
  }
rgw_export2={
    "daemon": "192.168.110.123",
    "clients": [
          "192.168.110.18",
          "192.168.110.12"
        ],
    "bucket": "y",
    "tag": "nfs01",
    "cluster_id": "_default_",
    "reload_daemons": True,
    "rgw_user_id": "admin"
  }
def test_rgw_export():  #测试rgw export接口
  #新的创建export接口
  
  print(send_request('POST',URL+'/api/nfs-ganesha/rgw-export', rgw_export, '创建rgw export'))
  #time.sleep(2)
  print(send_request('PUT',URL+'/api/nfs-ganesha/rgw-export/_default_/1', rgw_export2, '修改rgw export'))
  print(send_request('GET',URL+'/api/nfs-ganesha/rgw-export', '', '查看所有rgw export'))
  print(send_request('GET',URL+'/api/nfs-ganesha/rgw-export/_default_/1', '', '查看rgw export'))
  
def test_nfs_start():
  print(send_request('GET',URL+'/api/nfs-ganesha/daemon/_default_/1/start', '', '开启nfs服务'))
  print(send_request('GET',URL+'/api/nfs-ganesha/daemon/_default_/1/stop', '', '关闭nfs服务'))
  print(send_request('GET',URL+'/api/nfs-ganesha/rgw-export/status/_default_/1', '', '查看网关状态'))

def show_all_object(): #显示所有对象
  object_names = [obj.key for obj in ioctx.list_objects()]   #
  print('列出所有对象')
  print(object_names)
  for obj in ioctx.list_objects():
      size, _ = obj.stat()
      context = obj.read(size)
      context=context.decode("utf-8")
      print(obj.key+':', context)

def remove_all_object():
  for obj in ioctx.list_objects():
      ioctx.remove_object(obj.key)
      ioctx.write_full('conf-node1', ''.encode('utf-8'))

def test_host():  #测试获取hosts
  print(send_request('GET',URL+'/api/host/hostsaddr', '' , '获取集群所有节点的host'))
  print(send_request('GET',URL+'/api/host/rgwnfs', '' , '获取集群rgw-nfs节点的host'))
  print(send_request('GET',URL+'/api/host/rgwhosts', '' , '获取集群rgw节点的host'))

def test_rgw():
  print(send_request('GET',URL+'/api/rgw/user', '' , '获取rgw用户信息'))


#test_host()
#test_rgw()

#test_rgw_export()

#test_domain_name()
#test_nfs_start()
#print(send_request('POST',URL+'/api/nfs-ganesha/rgw-export', rgw_export, '创建rgw export'))
show_all_object()
remove_all_object()

#test_nfs_start()
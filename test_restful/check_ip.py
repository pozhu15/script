import re 

def check_ip(ip):
    searchip = re.search( r'^([0-9]{1,3}\.){3}[0-9]{1,3}$', ip)
    if searchip:
        return searchip;
    else:
        return False;

def check_domain(domian):
    searchip = re.search(r'[a-zA-Z]+://[^\s]*[.com|.cn]', ip)
    if searchip:
        return True;
    else:
        return False;

def  is_ipv4(ip):
    return True  if [1] * 4 == [x.isdigit() and 0 <= int(x) <= 255 for x in ip.split(".")] else False


print(check_ip(' 124.36.56.446'))

print(check_ip('124.36.56.446'))

print(is_ipv4('124.36.56.46'))

cd /jtest/Lakestor/build/src/mgr
make -j8
systemctl stop ceph-mgr@node2
make DESTDIR=/jtest/Lakestor/build/buildinstall install/strip 
rm /usr/bin/ceph-mgr
cp /jtest/Lakestor/build/buildinstall/usr/bin/ceph-mgr  /usr/bin/
systemctl restart ceph-mgr@node2
tail -f /var/log/ceph/ceph-mgr.node2.log
#删除ceph集群数据，重新部署ceph
cd /etc/ceph/
pkill ceph
ceph-deploy forgetkeys
rm -rf /etc/ceph/*
rm -rf /var/run/ceph/*
rm -rf /var/lib/ceph/*/*
rm -rf /var/log/ceph/*

mkfs.xfs -f /dev/sda3
ceph_data=`dmsetup ls |grep -o -E 'ceph--\S+'`
dmsetup remove $ceph_data
mkfs.xfs -f /dev/sda3

ceph-deploy new node0
echo "osd_pool_default_size = 1" >> ceph.conf
echo "mon_allow_pool_delete = true" >> ceph.conf

ceph-deploy mon create-initial

ceph-deploy admin node0
ceph-deploy mgr create node0
ceph-deploy osd create node0 --data /dev/sda3
#ceph-deploy gatherkeys node0
ceph-deploy rgw create node0
radosgw-admin user create --uid=admin --display-name=admin --access_key=admin --secret=admin

ceph-deploy admin node0
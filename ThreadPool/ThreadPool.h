/*
 * ThreadPool.h
 *
 *  Created on: 2019年5月23日
 *      Author: lgp
 */
#pragma once
#include <vector>
#include <utility>
#include <queue>
#include <thread>
#include <functional>
#include <mutex>
#include <condition_variable>

typedef std::function<void*(void*)> TaskFunc;
class ThreadPool
{
public:
	static int m_s_iThreadsSize;
	enum Priority { Level_High, Level_Mid, Level_Low};
	typedef struct Task
	{
		TaskFunc func;
		void* pUsrData;
	}TAKE;
	typedef std::pair<Priority, Task> TaskPair;
	ThreadPool(int iSize);
	~ThreadPool();
	void start();
	void stop();
	void addTask(const Task&);
	void addTask(TaskFunc, void* usrData);
	void addTask(const TaskPair&);
private:
	ThreadPool(const ThreadPool&);//禁止复制拷贝.
	const ThreadPool& operator=(const ThreadPool&);
	struct TaskPriorityCmp
	{
		bool operator()(const ThreadPool::TaskPair p1, const ThreadPool::TaskPair p2)
		{
			return p1.first > p2.first; //first的小值优先
		}
	};

	void threadLoop();
	Task take();
	typedef std::vector<std::thread*> Threads;
	//优先队列
	typedef std::priority_queue<TaskPair, std::vector<TaskPair>, TaskPriorityCmp> Tasks;
	Threads m_threads;
	Tasks m_tasks;

	std::mutex m_mutex;
	std::condition_variable m_cond;
	bool m_isStarted;
};


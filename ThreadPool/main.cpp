#include <iostream>
#include <chrono>
#include <condition_variable>
#include "ThreadPool.h"

int iID = 0;
std::condition_variable g_cond;
std::mutex g_wMutex;
void* Producer(void* usrData)
{
	std::queue<int>* veData = (std::queue<int>*)usrData;
	while (iID < 100)
	{
		{
			std::unique_lock<std::mutex> lock(g_wMutex);
			if (veData->size() <= 5)
			{
				std::cout << "生产线程[" << std::this_thread::get_id() << "]生产产品:" << ++iID << std::endl;
				veData->push(iID);
				g_cond.notify_one();
			}
			else
			{
				std::cerr << "仓库满" << std::endl;
				g_cond.wait(lock);		
			}				
		}

		std::this_thread::sleep_for(std::chrono::seconds(1));
	}
	return NULL;
}

void* Consumer(void* usrData)
{
	std::queue<int>* veData = (std::queue<int>*)usrData;
	int d = 0;
	while (d < 100)
	{
		{
			std::unique_lock<std::mutex> lock(g_wMutex);
			if (veData->size() != 0)
			{
				d = veData->front();
				veData->pop();
				std::cout << "消费线程[" << std::this_thread::get_id() << "]消费了产品:" << d << std::endl;
				g_cond.notify_one();
			}
			else
			{
				std::cerr <<"仓库空"<<std::endl;
				g_cond.wait(lock);
			}
				
		}

		std::this_thread::sleep_for(std::chrono::seconds(std::rand() % 3));
	}
	return NULL;
}
#include <ctime>
int main(int argc, char* argv[])
{
	std::srand(std::time(0));
	ThreadPool ThrPool(7);
	ThrPool.start();
	std::queue<int> veProducts;
	ThrPool.addTask(Producer, &veProducts);
	ThrPool.addTask(Producer, &veProducts);
	ThrPool.addTask(Producer, &veProducts);
	ThrPool.addTask(Consumer, &veProducts);
	ThrPool.addTask(Consumer, &veProducts);
	ThrPool.addTask(Consumer, &veProducts);
	ThrPool.addTask(Consumer, &veProducts);
	getchar();
	iID = 80;
	ThrPool.addTask(Producer, &veProducts);
	getchar();
	return 0;
}
/*
 * ThreadPool.cpp
 *
 *  Created on: 2019年5月23日
 *      Author: lgp
 */
#include <assert.h>
#include <iostream>
#include "ThreadPool.h"
int ThreadPool::m_s_iThreadsSize = 0;
ThreadPool::ThreadPool(int iSize = 3)
	:m_mutex(),
	m_cond(),
	m_isStarted(false)
{
	if (iSize == 0)
		m_s_iThreadsSize = 3;
	else
		m_s_iThreadsSize = iSize;
}
ThreadPool::~ThreadPool()
{
	if (m_isStarted)
	{
		stop();
	}
}
void ThreadPool::start()
{
	assert(m_threads.empty());
	m_isStarted = true;
	m_threads.reserve(m_s_iThreadsSize);
	for (int i = 0; i < m_s_iThreadsSize; ++i)
	{
		m_threads.push_back(new std::thread(std::bind(&ThreadPool::threadLoop, this)));
	}
}
void ThreadPool::stop()
{
	{
		std::unique_lock<std::mutex> lock(m_mutex);
		m_isStarted = false;
		m_cond.notify_all();
	}
	for (Threads::iterator it = m_threads.begin(); it != m_threads.end(); ++it)
	{
		(*it)->join();
		delete *it;
	}
	m_threads.clear();
}
void ThreadPool::threadLoop()
{
	while (m_isStarted)
	{
		Task task = take();
		if (task.func)
		{
			task.func(task.pUsrData);
		}
	}
}

void ThreadPool::addTask(const Task& task)
{
	std::unique_lock<std::mutex> lock(m_mutex);
	TaskPair taskPair(Level_Mid, task);
	m_tasks.push(taskPair);
	m_cond.notify_one();
}

void ThreadPool::addTask(TaskFunc pHandleFunc, void * usrData)
{
	std::unique_lock<std::mutex> lock(m_mutex);
	ThreadPool::Task t = { pHandleFunc, usrData };
	TaskPair taskPair(Level_Mid, t);
	m_tasks.push(taskPair);
	m_cond.notify_one();
}

void ThreadPool::addTask(const TaskPair& taskPair)
{
	std::unique_lock<std::mutex> lock(m_mutex);
	m_tasks.push(taskPair);
	m_cond.notify_one();
}
const ThreadPool& ThreadPool::operator=(const ThreadPool& tp)
{
	return *(new ThreadPool(3));
}

ThreadPool::Task ThreadPool::take()
{
	std::unique_lock<std::mutex> lock(m_mutex);
	while (m_tasks.empty() && m_isStarted)
	{
		m_cond.wait(lock);
	}
	Task task;
	Tasks::size_type size = m_tasks.size();
	if (!m_tasks.empty() && m_isStarted)
	{
		task = m_tasks.top().second;
		m_tasks.pop();
		assert(size - 1 == m_tasks.size());
	}
	return task;
}
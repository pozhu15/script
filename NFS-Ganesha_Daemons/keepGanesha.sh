if [ ! -d "/etc/cron_gs" ];then
  mkdir /etc/cron_gs
fi
cat>/etc/cron_gs/check_gs_ps.sh<<EOF
for ((i=1; i<=6; i++))
do
ps -fe|grep ganesha.nfsd|grep -v grep	#检测ganesha进程是否启动
if [ \$? -eq 0 ]
then
	echo \`date\` activing> /var/log/check_ganesha.txt
	#mount -a 
else		#没启动择执行下面的脚本进行启动ganesha
    echo \`date\` restart> /var/log/check_ganesha.txt
    ganesha.nfsd -f /etc/ganesha/ganesha.conf -L /var/log/nfs-ganesha.log -N NIV_DEBUG  #启动ganesha
    mount -a    #执行挂载
fi
if [ \$i -eq 6 ] ;then 
break
fi
sleep 10
done
EOF
chmod 777 /etc/cron_gs/check_gs_ps.sh


#### 配置自动挂载（/etc/fstab）协议及路径
```shell
vi /etc/fstab
#/etc/fstab中不同nfs协议挂载设置方式如下（node1为hosts中的主机名，/mnt/cephNfs为自己的挂载路径）
#将以下需要的挂载方式添加到/etc/fstab文件
node1:/  /mnt/cephNfs3 nfs defaults,vers=3 0 0  	# NFSv3
node1:/  /mnt/cephNfs4 nfs4 defaults,vers=4.0 0 0	# NFSv4
node1:/  /mnt/cephNfs4.1 nfs4 defaults,vers=4.1 0 0	# NFSv4.1
node1:/  /mnt/cephNfs4.2 nfs4 defaults,vers=4.2 0 0	# NFSv4.2
#
```
#### 添加nfa-ganesha守护进程并启动nfa-ganesha进程
```shell
chmod 777 keepGanesha.sh
./keepGanesha.sh	
#将脚本添加到定时任务
crontab -e 
#按 i，打开的文件编辑器里输入如下内容
* * * * * /etc/cron_gs/check_gs_ps.sh

#然后按Esc 输入 :wq 保存退出
#运行之后脚本10秒检测一次ganesha进程，如果没有启动会自动启动ganesha
```
#### 关闭和启动nfa-ganesha守护进程
```shell
chmod 777 stop_ganesha.sh
./stop_ganesha.sh 1	#关闭nfa-ganesha守护进程，并关闭nfa-ganesha进程
./stop_ganesha.sh 0	#启动nfa-ganesha守护进程，并启动nfa-ganesha进程
```

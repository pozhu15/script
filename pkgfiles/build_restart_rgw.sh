#在当前机器重新编译并安装rgw，前提是调试的rgw和ganesha在当前机器
current_path=$(cd "$(dirname $0)";pwd)
cd $current_path
./pkg.sh rgw  || exit	#编译打包rgw
./stop_ganesha.sh 1	#关闭nfa-ganesha守护进程，并关闭nfa-ganesha进程
ps -ef |grep ganesha.nfsd
rpm -ivh ~/rpmbuild/RPMS/x86_64/librgw2-*.rpm --nodeps --force --replacefiles	#安装rgw
./stop_ganesha.sh 0	#启动nfa-ganesha守护进程，并启动nfa-ganesha进程
echo "等待重启ganesha.nfsd"
while true
	do
		ps -fe|grep ganesha.nfsd|grep -v grep
      if [ $? -eq 0 ]
			then
				break
			else		
				sleep 1
			fi
    done 

echo "已重启ganesha.nfsd"
sleep 1
ps -ef |grep ganesha.nfsd
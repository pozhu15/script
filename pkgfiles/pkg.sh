name=rgw
version=14.2.0
rgw_name=$name-$version
ceph_name=ceph-Lakestor_v1.0.0.13

current_path=$(cd "$(dirname $0)";pwd)
cd $current_path
buildroot=$current_path/buildroot
#rpmbuild工具检测安装
yum list installed | grep rpm-devel
if [ $? -ne 0 ];then
	yum install rpm-devel.x86_64 -y
	yum install rpmdevtools -y
	rpmdev-setuptree
fi
rpath_check=`grep "^#.*%__arch_install_post" ~/.rpmmacros`
if [ x"$rpath_check" = x  ];then 
	echo "监测到没有关闭check-rpaths，将会使打包失败，请手动编辑文件：vi ~/.rpmmacros  "
	echo "#%__arch_install_post /usr/lib/rpm/check-rpaths /usr/lib/rpm/check-buildroot         #注释掉这行"
	read -p "注释完成按任意键继续" str1
fi
if [ $1 == "rgw" ];then #仅打包rgw
	cd ../Lakestor/build/src/rgw
	make -j2  ||exit
	make DESTDIR=$buildroot install  ||exit
	cd -
	rm -rf $rgw_name
	./strip.sh $buildroot
	cp -r $buildroot $rgw_name

	rm -rf ~/rpmbuild/BUILDROOT/*
	tar -czf $rgw_name.tar.gz $rgw_name
	cp $rgw_name.tar.gz ~/rpmbuild/SOURCES
	cp specfiles/rgw.spec  ~/rpmbuild/SPECS/
	rpmbuild -ba ~/rpmbuild/SPECS/rgw.spec ||exit
	echo "rpm包已生成在~/rpmbuild/RPMS路径下，请用以下命令安装对应的rpm包"
	echo "安装前关闭ganesha进程，安装完再启动"
	echo "rpm -ivh ~/rpmbuild/RPMS/x86_64/librgw2-*.rpm --nodeps --force --replacefiles"
elif [ $1 == "ceph" ];then  #会同时打包ceph-ganesha，单独rgw包	
	mkdir -p $buildroot/usr/lib/systemd/system-preset
	cp  ../Lakestor/systemd/50-ceph.preset $buildroot/usr/lib/systemd/system-preset/50-ceph.preset
	mkdir -p $buildroot/etc/logrotate.d/
	cp  ../Lakestor/src/logrotate.conf $buildroot/etc/logrotate.d/ceph
	mkdir -p $buildroot/etc/sysconfig
	cp  ../Lakestor/etc/sysconfig/ceph $buildroot/etc/sysconfig/ceph
	mkdir -p $buildroot/usr/share/doc/ceph
	cp  ../Lakestor/COPYING $buildroot/usr/share/doc/ceph/COPYING
	mkdir -p $buildroot/usr/lib/tmpfiles.d
	cp  ../Lakestor/systemd/ceph.tmpfiles.d $buildroot/usr/lib/tmpfiles.d/ceph-common.conf
	mkdir -p $buildroot/etc/ceph
	cp  ../Lakestor/src/etc-rbdmap $buildroot/etc/ceph/rbdmap
	mkdir -p $buildroot/usr/lib/udev/rules.d
	cp  ../Lakestor/udev/50-rbd.rules $buildroot/usr/lib/udev/rules.d/50-rbd.rules
	mkdir -p $buildroot/usr/lib/sysctl.d
	cp  ../Lakestor/etc/sysctl/90-ceph-osd.conf $buildroot/usr/lib/sysctl.d/90-ceph-osd.conf
	mkdir -p $buildroot/etc/sudoers.d/
	cp  ../Lakestor/sudoers.d/ceph-osd-smartctl $buildroot/etc/sudoers.d/ceph-osd-smartctl

	cd ../Lakestor/build/src/rgw
	cd ../..  	#取消这句注释会从头开始编译ceph，时间较长，如非必要，不要改变，buildroot目录已经编译好了ceph，打包ceph时初始的buildroot目录不要删除，打包ceph会使用buildroot中编译好的ganesha
	make -j4  ||exit
	make DESTDIR=$buildroot install  ||exit
	cd $current_path
	rm -rf $ceph_name
	./strip.sh $buildroot
	cp -r $buildroot $ceph_name

	rm -rf ~/rpmbuild/BUILDROOT/*
	tar -czf $ceph_name.tar.gz $ceph_name
	cp $ceph_name.tar.gz ~/rpmbuild/SOURCES
	cp specfiles/ceph-ganesha.spec  ~/rpmbuild/SPECS/
	rpmbuild -ba ~/rpmbuild/SPECS/ceph-ganesha.spec ||exit
	echo "rpm包已生成在~/rpmbuild/RPMS路径下，请用以下命令安装对应的rpm包"
	echo "安装前关闭相关进程"
	echo "rpm -ivh 名称*.rpm --nodeps --force --replacefiles"
	echo "注意目前打包的ceph在部署集群的时候有‘admin_socket: exception’异常还没有解决，请勿尝试安装"
fi


#### 解压pkgfiles.tar.gz，将pkgfiles文件夹复制到和源码Lakestor文件夹同级目录下

```shell
├── Lakestor
└── pkgfiles
```
#### 初始化编译环境
```shell
cd Lakestor
./install-deps.sh #初始化过就跳过
```
#### 执行cmake步骤
```shell
cd pkgfiles
./cmakepkg.sh	#若未改变Lakestor中的源码文件结构，执行一次即可，否则编译时会重新从头编译，比较慢
```
#### 编译打包rgw，ceph-ganesha
```shell
cd pkgfiles
./pkg.sh rgw	#单独打包rgw，生成~/rpmbuild/RPMS/x86_64/librgw2-14.2.0-0.el7.x86_64.rpm
./pkg.sh ceph	#将ceph，ganehsa，userspace-rcu打包进一个rpm包，生成ceph-ceph-Lakestor_v1.0.0.13-0.el7.x86_64.rpm，
			#librgw2_tmp-Lakestor_v1.0.0.13-0.el7.x86_64.rpm和./pkg.sh rgw生成的一样
```
#### 安装打包好的rpm包

>安装以下rpm包前关闭ganesha进程

```shell
#安装rgw的rpm包
rpm -ivh ~/rpmbuild/RPMS/x86_64/librgw2-14.2.0-0.el7.x86_64.rpm --nodeps --force --replacefiles  
#安装ceph-ganesha包，部署集群有遗留问题，如需调试请在空白机器安装测试
#空白机器安装ceph依赖
cd Lakestor
./install-deps.sh 
#另需要安装aws-sdk-cpp-1.0.164-1.el7.centos.x86_64.rpm，在’文件网关ganesha环境编译安装 v1.0.3/Lakestor-Package/Lakestor_required.tar、压缩包中可以找到
rpm -ivh ~/rpmbuild/RPMS/x86_64/ceph-ceph-Lakestor_v1.0.0.13-0.el7.x86_64.rpm --nodeps --force --replacefiles  
```
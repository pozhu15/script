#!/bin/bash
version=v1.1.0.0

sed -i "s/ceph-14.2.7/Lakestor_$version/g" Lakestor_${version}/ceph.spec
sed -i "s/14.2.7/Lakestor_$version/g" Lakestor_${version}/ceph.spec
sed -i "/git describe --match/d" Lakestor_${version}/make-dist
sed -i "/^version=/c version=Lakestor_$version" Lakestor_${version}/make-dist



rm -rf /root/rpmbuild/BUILD/*
rm -rf /root/rpmbuild/SPECS/*
rm -rf /root/rpmbuild/SOURCES/*
rm -rf /root/rpmbuild/RPMS/*


cp Lakestor_${version}/ceph.spec /root/rpmbuild/SPECS/
tar -cjf Lakestor_${version}.tar.bz2 Lakestor_$version
cp Lakestor_${version}.tar.bz2 /root/rpmbuild/SOURCES/


#scl enable devtoolset-8 bash
#cd /root/rpmbuild/SPECS
#rpmbuild -ba /root/rpmbuild/SPECS/ceph.spec



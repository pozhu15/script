#HDW密码
Pa$$w0rd@_
#访问外网
vi /etc/resolv.conf
#写入：
nameserver 114.114.114.114

#网关配置
cd /etc/sysconfig/network-scripts/ && ls
#查看网卡
ethtool ens9f0
ethtool ens9f1

vi ifcfg-ens9f1
#修改如下参数即可
BOOTPROTO=static
ONBOOT=yes
IPADDR=172.19.100.194
NETMASK=255.255.255.0

#重启网卡
ifdown ens9f1
ifup ens9f1

pkill radosgw
pkill ceph 
pkill ceph-crash 
pkill ceph-mon 
pkill ceph-mgr 
pkill ceph-osd 
ceph-deploy forgetkeys
umount -l `mount |grep ceph |awk '{print $3}'`
rm -rf /etc/ceph/*
rm -rf /var/run/ceph/*
rm -rf /var/lib/ceph/*/*
rm -rf /var/log/ceph/*
cd /etc/ceph
#ceph-deploy purge node2 node3  

/home/delete_ceph_lvm.sh
cat >/home/delete_ceph_lvm.sh <<eof
#!/bin/bash
lv_array=(`lvs | grep osd-block | awk '{print "/dev/"$2"/"$1}'`)
for lv in ${lv_array[@]};do
#echo ${lv}
lvremove ${lv} --yes
done
vg_array=(`vgs | grep ceph | awk '{print $1}'`)
for vg in ${vg_array[@]};do
#echo ${vg}
vgremove ${vg} --yes
done
pv_array=(`pvs | grep lvm | awk '{print $1}'`)
for pv in ${pv_array[@]};do
echo ${pv}
pvremove ${pv}
done
eof

cd /etc/ceph
ceph-deploy new  node3  --cluster-network 172.19.100.0/24 --public-network 172.38.40.0/24  #网关配置
#ceph-deploy new  node2 
ceph-deploy mon create-initial

scp *.keyring root@node2:/etc/ceph/
scp *.keyring root@node3:/etc/ceph/

echo "mon_allow_pool_delete = true" >> ceph.conf
echo "rgw_override_bucket_index_max_shards = 256" >> ceph.conf
echo "objecter_inflight_op_bytes = 2097152000" >> ceph.conf
echo "objecter_inflight_ops = 8192" >> ceph.conf
echo "rgw_enable_usage_log = true" >> ceph.conf
echo "rgw_rest_getusage_op_compat = true" >> ceph.conf
echo "osd_crush_update_on_start = false" >> ceph.conf  


scp ceph.conf root@node2:/etc/ceph/ 
scp ceph.conf root@node3:/etc/ceph/ 


cat >/home/osd.sh <<eof
#!/bin/bash
disk_array=(/dev/sda /dev/sdb /dev/sdc /dev/sdd /dev/sde /dev/sdf /dev/sdg /dev/sdh /dev/sdi /dev/sdj /dev/sdk /dev/sdl /dev/sdm /dev/sdn)
cd /etc/ceph/
for str in ${disk_array[@]};do
ceph-deploy osd create node2 --data $str
done
for str in ${disk_array[@]};do
ceph-deploy osd create node3 --data $str
done
eof

/home/osd.sh

# crush rule 分组
#创建一个新桶ssd_root，级别是root最高级
ceph osd crush add-bucket ssd_root root

#查看SSD的osd
ceph osd tree
#创建一个新的桶叫hostname-2  hostname 替换对应主机的hostname
ceph osd crush add-bucket node1-1 host root=ssd_root
#添加对应host下的ssd (通过ceph osd tree 查看每个节点下ssd的osd_id及WEIGHT)
ceph osd crush set osd.0 0.87299 host=node1-1 root=ssd_root
ceph osd crush set osd.1 0.87299 host=node1-1 root=ssd_root

#创建一个新的桶叫hostname-2  hostname 替换对应主机的hostname
ceph osd crush add-bucket node2-1 host root=ssd_root
#添加对应host下的ssd (通过ceph osd tree 查看每个节点下ssd的osd_id及WEIGHT)
ceph osd crush set osd.14 0.87299 host=node2-1 root=ssd_root
ceph osd crush set osd.15 0.87299 host=node2-1 root=ssd_root

#创建一个新的桶叫hostname-2  hostname 替换对应主机的hostname
ceph osd crush add-bucket node3-1 host root=ssd_root
#添加对应host下的ssd (通过ceph osd tree 查看每个节点下ssd的osd_id及WEIGHT)
ceph osd crush set osd.28 0.87299 host=node3-1 root=ssd_root
ceph osd crush set osd.29 0.87299 host=node3-1 root=ssd_root

#创建replicated规则
ceph osd crush rule create-replicated ssd_rule ssd_root host
#配置规则参数（每执行完一步执行ceph -s 命令查看状态是否OK

#创建副本pool

ceph osd pool create default.rgw.meta 32 32 ssd_rule
ceph osd pool create default.rgw.log 32 32 ssd_rule
ceph osd pool create .rgw.root 32 32 ssd_rule
ceph osd pool create default.rgw.control 32 32 ssd_rule
ceph osd pool create default.rgw.buckets.index 128 128  ssd_rule
ceph osd pool create default.rgw.buckets.cache 128 128 ssd_rule
ceph osd pool create default.rgw.buckets.non-ec 128 128  ssd_rule

ceph osd pool create default.rgw.buckets.data 128 128 


ceph osd pool create default.rgw.meta 8 8 
ceph osd pool create default.rgw.log 8 8 
ceph osd pool create .rgw.root 8 8 
ceph osd pool create default.rgw.control 8 8 
ceph osd pool create default.rgw.buckets.index 64 64  
ceph osd pool create default.rgw.buckets.cache 64 64  
ceph osd pool create default.rgw.buckets.non-ec 64 64  

ceph osd pool create default.rgw.buckets.data 64 64  
ceph osd pool create ganesha_data 32

ceph-deploy mgr create node1              #创建mgr ceph-node1为主机名称
ceph-deploy rgw create node1 node2 node3 
radosgw-admin user create --uid=admin --display-name=admin --access_key=admin --secret=admin
radosgw-admin user info --uid admin

#创建纠删pool
#查看默认规则
ceph osd erasure-code-profile ls
ceph osd erasure-code-profile set ec-profile K=2 M=1 runleset-failure-domain=host  crush-root=default
ceph osd crush rule create-erasure ec-rule ec-profile 
ceph osd pool create default.rgw.buckets.data 512 512 erasure ec-profile ec-rule
ceph osd pool set default.rgw.buckets.data allow_ec_overwrites 1 --yes-i-really-mean-it

#查看集群状态
ceph health detail
#按提示设置如下pool
ceph osd pool application enable default.rgw.control rgw
ceph osd pool application enable default.rgw.meta rgw
ceph osd pool application enable default.rgw.log rgw
ceph osd pool application enable default.rgw.buckets.index rgw
ceph osd pool application enable default.rgw.log rgw
ceph osd pool application enable default.rgw.buckets.cache rgw
ceph osd pool application enable default.rgw.buckets.non-ec rgw

ceph osd pool application enable default.rgw.buckets.data rgw
ceph osd pool application enable .rgw.root rgw
ceph osd pool application enable ganesha_data rgw

#附加
#设置副本pool数量
#systemctl stop ceph-radosgw@rgw.node1
#无法删除pool时node1重启mon
systemctl restart ceph-mon.target
#删除pool
ceph osd pool rm default.rgw.buckets.data default.rgw.buckets.data  --yes-i-really-really-mean-it  #在删除pool的时候要停止下rgw
#创建副本pool
ceph osd  pool create default.rgw.buckets.data 512  #默认三副本
#设置pool副本数
ceph osd pool set default.rgw.buckets.data size 2
#设置degrade最小副本数
ceph osd pool set default.rgw.buckets.data min_size 1
#查看副本数
ceph osd pool get default.rgw.buckets.data size

#查看存储池规则
ceph osd pool get default.rgw.buckets.data all
#重启rgw
systemctl start ceph-radosgw@rgw.node1

#删除所有pool
ceph osd lspools
ceph osd pool rm default.rgw.control default.rgw.control  --yes-i-really-really-mean-it
ceph osd pool rm rgw.buckets.index rgw.buckets.index  --yes-i-really-really-mean-it
ceph osd pool rm default.rgw.buckets.cache default.rgw.buckets.cache  --yes-i-really-really-mean-it
ceph osd pool rm default.rgw.buckets.data default.rgw.buckets.data  --yes-i-really-really-mean-it

ceph osd pool rm default.rgw.meta default.rgw.meta  --yes-i-really-really-mean-it
ceph osd pool rm default.rgw.log default.rgw.log  --yes-i-really-really-mean-it
ceph osd pool rm default.rgw.buckets.index default.rgw.buckets.index  --yes-i-really-really-mean-it
ceph osd pool rm default.rgw.buckets.non-ec default.rgw.buckets.non-ec  --yes-i-really-really-mean-it
ceph osd pool rm .rgw.root .rgw.root  --yes-i-really-really-mean-it
ceph osd pool rm ganesha_data ganesha_data  --yes-i-really-really-mean-it
ceph osd pool rm rgw.archive.data rgw.archive.data  --yes-i-really-really-mean-it

# 单节点设置单副本

ceph osd pool set  default.rgw.meta min_size 1
ceph osd pool set  default.rgw.log min_size 1
ceph osd pool set  .rgw.root min_size 1
ceph osd pool set  default.rgw.control min_size 1
ceph osd pool set  default.rgw.buckets.index min_size 1
ceph osd pool set  default.rgw.buckets.cache min_size 1
ceph osd pool set  default.rgw.buckets.non-ec min_size 1
ceph osd pool set  default.rgw.buckets.data min_size 1
ceph osd pool set  ganesha_data min_size 1
ceph osd pool set  rgw.archive.data min_size 1



#
ceph osd pool set  default.rgw.meta size 1
ceph osd pool set  default.rgw.log size 1
ceph osd pool set  .rgw.root size 1
ceph osd pool set  default.rgw.control size 1
ceph osd pool set  default.rgw.buckets.index size 1
ceph osd pool set  default.rgw.buckets.cache size 1
ceph osd pool set  default.rgw.buckets.non-ec size 1
ceph osd pool set  default.rgw.buckets.data size 1
ceph osd pool set  ganesha_data size 1
ceph osd pool set  rgw.archive.data size 1

# 设置pg数
ceph osd pool set  default.rgw.meta pg_num 8
ceph osd pool set  default.rgw.log pg_num 8
ceph osd pool set  .rgw.root pg_num 8
ceph osd pool set  default.rgw.control pg_num 8
ceph osd pool set  default.rgw.buckets.index pg_num 8
ceph osd pool set  default.rgw.buckets.cache pg_num 8
ceph osd pool set  default.rgw.buckets.non-ec pg_num 8
ceph osd pool set  default.rgw.buckets.data pg_num 8
ceph osd pool set  ganesha_data pg_num 8

# 设置pg数
ceph osd pool set  default.rgw.meta pgp_num 8
ceph osd pool set  default.rgw.log pgp_num 8
ceph osd pool set  .rgw.root pgp_num 8
ceph osd pool set  default.rgw.control pgp_num 8
ceph osd pool set  default.rgw.buckets.index pgp_num 8
ceph osd pool set  default.rgw.buckets.cache pgp_num 8
ceph osd pool set  default.rgw.buckets.non-ec pgp_num 8
ceph osd pool set  default.rgw.buckets.data pgp_num 8
ceph osd pool set  ganesha_data pgp_num 8

# 查看pool信息
ceph osd dump|grep pool

# 查看桶
radosgw-admin user list
radsogw-admin bucket stats --bucket=xxx
radosgw-admin bucket list --uid xxx获取用户名下的桶



#安装Dashboard
yum install -y ceph-mgr-dashboard -y

#开启插件
ceph mgr module enable dashboard

#禁用SSL。
ceph config set mgr mgr/dashboard/ssl false

#配置监听IP。
ceph config set mgr mgr/dashboard/server_addr 0.0.0.0
#ceph config-key put mgr/dashboard/server_addr  #或这个，未测试
#温馨提醒：此处必须设置监控地址为0.0.0.0,而不能是直接IP地址，因为其监控的是所有本地地址包括IPV4和IPV6，同时也不能禁用IPV6地址。

#配置监听端口。
ceph config set mgr mgr/dashboard/server_port 8444

#设置用户及密码。
 ceph dashboard ac-user-create admin admin administrator
{"username": "admin", "lastUpdate": 1605177808, "name": null, "roles": ["administrator"], "password": "$2b$12$kJnehyIaGpCXP.vl4wDE4un6Fy8PpioFJltIfiSqr9cuSq5YiWbe6", "email": null}

#使用配置生效。

ceph mgr module disable dashboard ; ceph mgr module enable dashboard


#查看已开启模块信息。
#通过查看ceph mgr services命令输出地址
ceph mgr services
{
    "dashboard": "http://ceph-node1:8444/"
}
## 查看已开启的模块
ceph mgr module ls | jq .enabled_modules

自定义插件
ceph mgr module enable hello --force

ceph tell mgr hello

ceph mgr module disable hello
ceph mgr module ls
init-ceph restart mgr

#Dashboard中启用RGW

radosgw-admin user create --uid=admin --display-name=admin --system
ceph dashboard set-rgw-api-access-key admin
ceph dashboard set-rgw-api-secret-key admin

# 查看
ceph dashboard get-rgw-api-access-key
ceph dashboard get-rgw-api-secret-key

# 启用orchestrator
[Orchestrator模块](https://blog.csdn.net/younger_china/article/details/99180359)

## 激活后端Orchestrator模块：
ceph mgr module enable <module>
ceph orchestrator set backend <module>
## 其中module是ansible、deepsea、rook或ssh.

ceph orchestrator set backend ssh

## 检查是否使用status命令正确配置了后端
ceph orchestrator status

## 查看已发现设备的列表
ceph orchestrator device ls

ceph orchestrator apply nfs foo ganesha_data nfs-ns

[Dashboard全功能安装集成](https://blog.csdn.net/renlixing87/article/details/105698800)
#Dashboard中启用ganesha

cd /etc/ceph/
echo "rgw_enable_usage_log = true " >> ceph.conf  
scp ceph.conf root@node2:/etc/ceph/ 
scp ceph.conf root@node3:/etc/ceph/ 
ceph osd pool create ganesha_data 32

# 单节点设置单副本
ceph osd pool set ganesha_data min_size 1
ceph osd pool set ganesha_data size 1

ceph osd pool application enable ganesha_data rgw

ceph osd pool set cephfs_datapool min_size 1
ceph osd pool set cephfs_datapool size 1

#新建空的daemon.txt文本文件。
touch daemon.txt
#导入daemon文件到ganesha_data pool中。
rados -p ganesha_data put conf-node1 daemon.txt  #创建一个名为conf-node1的对象，将本地文件daemon.txt拷贝到这个pool的对象
rados -p ganesha_data ls

#引申其他命令 
#rados get conf-node1 /jtest/getfile -p ganesha_data   #将上传的file文件拷贝到本机，重命名为getfile 
#ceph osd map ganesha_data conf-node1 #查看对象的pg map

ceph health detail



查看gaensha pool中存在conf-node1
rados -p ganesha_data ls

touch daemon.txt
rados -p ganesha_data -N ganesha-export-index put conf-node1 daemon.txt
ceph dashboard set-ganesha-clusters-rados-pool-namespace ganesha_data/ganesha-export-index
ceph mgr module disable dashboard ; ceph mgr module enable dashboard

# 查看pool下的对象
rados -p ganesha_data -N ganesha-export-index ls
# 删除pool下的对象
rados -p ganesha_data -N ganesha-export-index rm domain_names-2


#vi /etc/ganesha/ganesha.conf
RADOS_URLS {
    ceph_conf = "/etc/ceph/ceph.conf";
    Userid = "admin";
    watch_url = "rados://ganesha_data/conf-node1";
}
%url rados://ganesha_data/conf-node1
RGW {
        ceph_conf = "/etc/ceph/ceph.conf";
        name = "client.rgw.ceph-node1.localdomain";
        cluster = "ceph";
}

#要在Ceph仪表板中启用NFS-Ganesha管理，我们只需要告诉仪表板要导出哪个pool,比如以下是导出cephfs_data pool。
ceph dashboard set-ganesha-clusters-rados-pool-namespace ganesha_data

#此处导出目录必须是前面创建的ganesha_data存储池，也就是导出的池必须有conf-xxx文件，在Dashboard中才能显示。
#重启ganesha服务，并设置开机启动。
systemctl restart nfs-ganesha
systemctl status nfs-ganesha

ceph mgr module disable dashboard ; ceph mgr module enable dashboard

ceph dashboard debug enable


## 自测版配置prometheus+grafana
cd /opt
sudo wget https://github.com/prometheus/prometheus/releases/download/v2.25.0/prometheus-2.25.0.linux-amd64.tar.gz
tar xvf prometheus-2.25.0.linux-amd64.tar.gz
cd prometheus-2.25.0.linux-amd64
vi  prometheus.yml
添加如下
```shell
  - job_name: "node1"
    static_configs:
    - targets: ["localhost:9283"]
```
./prometheus
http://172.38.40.193:9090

wget https://dl.grafana.com/oss/release/grafana-7.4.3-1.x86_64.rpm
sudo yum install grafana-7.4.3-1.x86_64.rpm
systemctl start grafana-server
http://172.38.40.193:3000
admin admin

## mgr日志级别设置
 ceph daemon /var/run/ceph/ceph-mgr.node1.asok config set debug_mgr 16

## 启动mgr-restful
ceph mgr module enable restful
#生成自签名证书：
ceph restful create-self-signed-cert
 curl -k https://localhost:8003/

#如果您具有签名正确的证书，请使用以下命令应用它们：
openssl req -new -nodes -x509   -subj "/O=IT/CN=ceph-mgr-restful"   -days 3650 -keyout restful.key -out restful.crt -extensions v3_ca
ceph config-key set mgr/restful/crt -i restful.crt
ceph config-key set mgr/restful/key -i restful.key


# 创建一个名为api的用户：
 ceph restful create-key api

 # 使用以下命令检查用户名和密钥：
ceph restful list-keys
 
# 凭证生成后，您可以使用curl来验证API访问权限，命令语法为：
https://<username>:<password-key>@<mgr-node>:<APIPort>/<REQUEST>
# 这是一个例子：
curl -k https://api:ecc28c4c-2564-4bbb-b04c-0d4b4868ee82@172.38.40.193:8003/server

curl -X POST "http://172.38.40.193:8444/api/auth"   -H  "Accept: application/vnd.ceph.api.v1.0+json"   -H  "Content-Type: application/json"   -d '{"username": "admin", "password": "123456"}'


#ceph restful api 使用




## 查看export
rados -p ganesha_data ls
rados get export-1 /jtest/export.txt -p ganesha_data 
cat /jtest/export.txt

rados get Expand_export-1 /jtest/export.txt -p ganesha_data 
cat /jtest/export.txt

rados get conf-node1 /jtest/conf-node1.txt -p ganesha_data
cat /jtest/conf-node1.txt


## token
curl -X POST "http://172.38.40.194:8444/api/auth"   -H  "Accept: application/vnd.ceph.api.v1.0+json"   -H  "Content-Type: application/json"   -d '{"username": "admin", "password": "123456"}'

curl -X POST "https://172.38.30.6:8443/api/auth"   -H  "Accept: application/vnd.ceph.api.v1.0+json"   -H  "Content-Type: application/json"   -d '{"username": "admin", "password": "admin"}'


# token
curl -X POST "http://node3:8444/api/auth"   -H  "Accept: application/vnd.ceph.api.v1.0+json"   -H  "Content-Type: application/json"   -d '{"username": "admin", "password": "admin"}'

# https token
curl -X POST "http://node3:8444/api/auth"   -H  "Accept: application/vnd.ceph.api.v1.0+json"   -H  "Content-Type: application/json"   -d '{"username": "admin", "password": "admin"}' -k --tlsv1 

# rgw restful http 
curl -X GET "http://172.38.30.2:7480/admin/bucket" -H  "Content-Type: application/json" -d '{"access_key:admin", "service_base_url":"//172.38.30.2:7480", "secret_key":"admin"}'
# params:{'bucket': u'07261t-d'}  self.auth:['access_key:admin', 'service_base_url://172.38.30.2:7480', 'secret_key:admin']

# mds配置


# 部署MDS服务
ceph-deploy mds create node1 node2 #在每个节点上创建MDS服务
# 创建文件系统元数据池
ceph osd pool create cephfs-metadata 8 #Pool池创建详细配置参考pool创建指导

ceph osd pool set cephfs-metadata min_size 1
ceph osd pool set cephfs-metadata size 1
# 创建文件系统数据池
ceph osd pool create cephfs-data 64

ceph osd pool set cephfs-data min_size 1
ceph osd pool set cephfs-data size 1
# 创建文件系统
ceph fs new cephfs cephfs-metadata cephfs-data


# 在 Monitor 上, 创建一个用户，用于访问CephFs
ceph auth get-or-create client.cephfs mon 'allow rw' mds 'allow rw' osd 'allow rw pool=cephfs-data, allow rw pool=cephfs-metadata'
[client.cephfs]
	key = AQAz4NtgElsBARAA6k9eDbhC6P18rKvXHKxpSw==

# 验证key是否生效
ceph auth get client.cephfs

exported keyring for client.cephfs
[client.cephfs]
	key = AQDQTdFg8uNqCxAAu2qgtghIFtO/UxdsZPXneA==
	caps mds = "allow rw"
	caps mon = "allow rw"
	caps osd = "allow rw pool=cephfs-data, allow rw pool=cephfs-metadata"

# CephFS 权限与访问控制
# 获取权限列表
ceph auth list
# 删除某个用户
ceph auth del client.lucy
# 获取某个用户的key
ceph auth get-key client.lucy
AQCjtyxbVNbUHBAAii6mTXjOTLIG7K3p7RRDIQ==
# 修改用户权限
ceph auth caps client.lucy mon 'allow rw' mds 'allow rw, allow rw path=/lucy, allow rw path=/jerry/jerry_share' osd 'allow rw'

# 检查CephFs和mds状态
ceph mds stat
ceph fs ls
ceph fs status
ceph fs status cephfs

# 挂载目录
mkdir /mnt/cephfs
chmod 777 -R /mnt/cephfs/
ceph auth get client.cephfs #查看admin用户的key，填入下面的命令
mount -t ceph 192.168.110.193:6789:/ /mnt/cephfs/ -o name=cephfs,secret=AQD8UiRhYjUQFBAAagPAuB0J2INzxr88uLWpCg==
# 秘钥就是上面创建用户的key

df -h
# 192.168.25.224:6789,192.168.25.227:6789,192.168.25.228:6789:/   36G  9.2G   27G  26% /cephfs

# 自动挂载
echo "mon1:6789,mon2:6789,mon3:6789:/ /cephfs ceph name=cephfs,secretfile=/etc/ceph/cephfs.key,_netdev,noatime 0 0" | sudo tee -a /etc/fstab

# 验证是否挂载成功
stat -f /mnt/cephfs/

# 脚本部署
ceph-deploy create mds node3
python /usr/share/ceph/mgr/dashboard/services/cephfs_deploy.py

# 查看文件系统配额：
getfattr -n ceph.quota.max_files /mnt/cephfs/fs1
getfattr -n ceph.quota.max_bytes /mnt/cephfs/fs1



# 配置Samba对接cephfs 
# 安装Samba
yum -y install samba samba-client samba-common

# 查看版本
rpm -qi samba

# 创建samba用户
groupadd samba
useradd samba -d /home/samba -g smb -s /sbin/nologin
smbpasswd -a samba  # 设置密码123456

# 启动smb服务
systemctl start smb.service
systemctl restart smb.service
systemctl enable smb.service

# 查看smb状态
systemctl status smb.service

# 测试smb连通性
smbclient -L localhost -U


# 用户可以利用samba的vfs-ceph插件实现samba协议和文件系统的对接。
# 首先配置samba的配置文件 /etc/samba/smb.conf
# 其中：
  - cephcc：该实例名称
  - comment：该实例的说明
  - vfs objects：samba插件的名称，必须要是ceph
  - ceph:config_file：ceph配置文件

[cephcc]
      comment = ceph file system
      vfs objects = ceph
      browseable = yes
      writeable = yes
      path = /             #共享路径
      ceph:config_file = /etc/ceph/ceph.conf
      ceph:user_id = Lakers
      kernel share modes = No
      read only = No
      oplocks = No
      valid users = sambauser root      #有访问权限的用户
      write list = sambauser root       #有写权限的用户


# 本机
[fs13]
	comment=ceph file system
	browseable=yes
	oplocks=No
	valid users=admin root samba
	kernel share modes=No
	write list=admin root samba
	path=/mnt/cephfs/
	read only=No
	writeable=yes
  force directory mask=0775
  force create mask=0775
[global]
	log file=/var/log/samba/log.%m
	server string=Linux Samba Version %v
	security=user
	workgroup=WORKGROUP


# 检查配置文件正确性
testparm
testparm –v  # 命令可以详细的列出smb.conf支持的配置参数

# 将共享文件夹的用户和组改为nobody，权限改为777：
# 确定nobody的组名用户名都叫nobody
$ id nobody
uid=99(nobody) gid=99(nobody) groups=99(nobody)

# 设置权限
chown -R nobody:nobody /mnt/cephfs/
chmod -R 777 /mnt/cephfs/

# 接着可以启动ganesha后可以通过nfs协议的挂载命令执行挂载。
# 创建用户Lakers，并将其keyring文件放在 /etc/ceph 下
cd /etc/ceph
ceph auth get-or-create client.Lakers mon 'allow rw' osd 'allow rw' mds 'allow rw' -o ceph.client.Lakers.keyring

# 创建samba账户并激活
useradd user1 -s /sbin/nologin
smbpasswd -a user1 # 设置密码123456
smbpasswd -e user1

useradd samba -s /sbin/nologin -p 123456  # 创建系统用户samba，密码123456
passwd=123456 && printf "${passwd}\n${passwd}\n"  | smbpasswd -a samba # 创建samba用户samba，密码123456
smbpasswd -e samba

# 查看系统用户

### 在创建新用户时，将修改以下 4 个文件。
* `/etc/passwd`： 用户账户的详细信息在此文件中更新。
* `/etc/shadow`： 用户账户密码在此文件中更新。
* `/etc/group`： 新用户群组的详细信息在此文件中更新。
* `/etc/gshadow`： 新用户群组密码在此文件中更新。

# 查看用户列表
awk -F':' '{ print $1}' /etc/passwd 

# 删除用户
userdel -r user1

# 重新启动samba服务
systemctl restart smb.service
systemctl status smb.service

# 查看日志信息
smbd -F -S

# 用户相关命令
useradd samba -s /sbin/nologin   # 增加系统用户
#passwd samba       # 设置系统密码
smbpasswd -a samba # 增加用户（要增加的用户必须以是系统用户,设置密码）
smbpasswd -d samba # 冻结用户，就是这个用户不能在登录了
smbpasswd -e samba # 恢复用户，解冻用户，让冻结的用户可以在使用
smbpasswd -n samba # 把用户的密码设置成空.
smbpasswd -x samba # 删除用户


useradd samba2 -s /sbin/nologin   # 增加系统用户
smbpasswd -a samba2 # 增加用户（要增加的用户必须以是系统用户,设置密码）
smbpasswd -e samba2 

# 在windows端的资源目录上直接访问
\\192.168.110.187\fs1

# 或者在linux客户端上用以下命令挂载
mount -t cifs -o username=user1,password=123456 //192.168.110.187/fs1 /mnt/samba

# 检查共享文件列表
smbclient -L  //192.168.110.187/

systemctl restart smb.service

#systemctl restart smb

# keepalived高可用组配置

### 节点安装keepalived
yum install -y keepalived

#配置keepalived.conf
vi /etc/keepalived/keepalived.conf

global_defs {
   notification_email {
     admin@example.com
   }
   notification_email_from noreply_admin@example.com
   smtp_server 127.0.0.1
   smtp_connect_timeout 60
   script_user root        #指定运行脚本的用户名和组为root
   enable_script_security  #启动脚本安全认证
}

vrrp_instance VI_MGR_SERVISE {                      # 实例名称
    state BACKUP                                # 可以是MASTER或BACKUP，不过当其他节点keepalived启动时会将priority比较大的节点选举为MASTER
    interface ens33                             # 节点固有IP（非VIP）的网卡，用来发VRRP包做心跳检测
    virtual_router_id 67           # 虚拟路由ID,取值在0-255之间,用来区分多个instance的VRRP组播,同一网段内ID不能重复;主备必须为一样;
    priority 100                                # 用来选举master的,要成为master那么这个选项的值最好高于其他机器50个点,该项取值范围是1-255(在此范围之外会被识别成默认值100)
    advert_int 1                                # 检查间隔默认为1秒,即1秒进行一次master选举(可以认为是健康查检时间间隔)
    authentication {                            # 认证区域,认证类型有PASS和HA（IPSEC）,推荐使用PASS(密码只识别前8位)
        auth_type PASS                          # 默认是PASS认证
        auth_pass 100                           # PASS认证密码
    }
    virtual_ipaddress {
        192.168.110.200
    }
}

#启动keepalived
systemctl restart keepalived
systemctl enable keepalived

#测试虚IP
ping  192.168.110.200

#查看节点1 IP信息
ip addr show ens33

#停止节点1服务，测试节点2虚IP漂移
systemctl stop keepalived
#查看节点2 IP信息
ip addr show ens33  

#动态重载keepalived配置
kill -HUP $(cat /var/run/keepalived.pid)


# centos命令行发送邮件

# mail是否已安装
 rpm -qa|grep mail

 # 安装
 yum install sendmail
 yum install mailx –y
 yum update libreport-plugin-mailx
 yum -y install sharutils
 yum install mutt

# 修改配置文件，配置邮件相关内容（追加到配置文件末尾）
vi /etc/mail.rc
后面加入
```
set from=pozhu15@qq.com
set smtp=smtp.qq.com  #邮件服务器
set smtp-auth-user=pozhu15@qq.com
set smtp-auth-password=vekxymtkjykyfdgj   #登录qq邮箱，设置->账户->开启POP3/IMAP/SMTP，生成的授权码填到这
set smtp-auth=login
```

# 命令行发送邮件
echo  消息内容 | mail -s "title" pozhu15@qq.com

echo  异常了，快来看看 | mail -s "来自服务器的提醒" 1249223183@qq.com


# 配置placement
##	创建placement，其中ehl-placement为要指定的placement的id
radosgw-admin zonegroup placement add --rgw-zonegroup=default  --placement-id=ehl-placement

##	初始化placement，可以直接从其他placement拷贝
radosgw-admin zone placement copy --rgw-zone default  --placement-id ehl-placement --src-placement-id default-placement
## 或者手动配置，手动配置需要指定相应的存储池如数据池、索引池、缓存池、数据额外池和元数据池。
radosgw-admin zone placement add --rgw-zone default --placement-id ehl-placement --data-pool default.rgw.buckets.data --index-pool default.rgw.buckets.index --data-extra-pool default.rgw.buckets.non-ec --cache-pool default.rgw.buckets.cache --meta-pool default.rgw.buckets.meta

# 蓝光配置

"access_key": "9oQXxw9EH6H3hW8n",
        "secret_key": "IEANjzOLHUbhacMtsmYp53vIMWcmCAjo",
        "url": "172.38.70.32:9000",
 bucket:"tdashb"
##	配置一个外部存储library
radosgw-admin library create --library-id lib001 --sc-type Glacier-G --external-url 172.38.70.32:9000 --access-key 9oQXxw9EH6H3hW8n --secret-key IEANjzOLHUbhacMtsmYp53vIMWcmCAjo

##	为该library添加bucket
radosgw-admin library addbucket --library-id lib001 --external-device tdashb

##	添加GLACIER类型的storage-class，指定placement-id和storage-class类型
radosgw-admin zonegroup placement add --rgw-zonegroup default --placement-id ehl-placement --storage-class GLACIER

##	将该library加入到placement中，指定存储类型、分配权重，library-weight默认值均为100，如果配置多个library，则可根据需要配置library-weight进行均衡【未指定则均分】
radosgw-admin zone placement external-add --rgw-zone default --placement-id ehl-placement --external-storage-class GLACIER --library-id lib001【--library-weight 100】

##	将library信息和placement信息更新到网关实例上【每个节点都要执行,不需要重启网关】
ceph daemon /var/run/ceph/ceph-client.rgw.node1.asok reload library lib001
ceph daemon /var/run/ceph/ceph-client.rgw.node1.asok reload placement ehl-placement


radosgw-admin zonegroup placement add --placement-id default-placement --storage-class GLACIER
radosgw-admin zone placement external-add --placement-id default-placement  --external-storage-class GLACIER --library-id lib001 

##	将library信息和placement信息更新到网关实例上【每个节点都要执行,不需要重启网关】
ceph daemon /var/run/ceph/ceph-client.rgw.node3.asok reload library lib001
ceph daemon /var/run/ceph/ceph-client.rgw.node3.asok reload placement ehl-placement

## 重启
 systemctl restart ceph-radosgw@rgw.node3
 
## 这两个可以看是否配置好
radosgw-admin library dump
radosgw-admin zone get


# 删除osd

# 调整osd的crush weight
ceph osd crush reweight osd.28 0.1
# 说明：这个地方如果想慢慢的调整就分几次将crush 的weight 减低到0 ，这个过程实际上是让数据不分布在这个节点上，让数据慢慢的分布到其他节点上，直到最终为没有分布在这个osd，并且迁移完成
# 这个地方不光调整了osd 的crush weight ，实际上同时调整了host 的 weight ，这样会调整集群的整体的crush 分布，在osd 的crush 为0 后， 再对这个osd的任何删除相关操作都不会影响到集群的数据的分布

# 停止osd进程
ceph stop osd.28  # 不起作用
systemctl stop ceph-osd@28

# 停止到osd的进程，这个是通知集群这个osd进程不在了，不提供服务了，因为本身没权重，就不会影响到整体的分布，也就没有迁移
# 将节点状态标记为out
ceph osd out osd.28

# 停止到osd的进程，这个是通知集群这个osd不再映射数据了，不提供服务了，因为本身没权重，就不会影响到整体的分布，也就没有迁移
# 从crush中移除节点
ceph osd crush remove osd.28

# 这个是从crush中删除，因为已经是0了 所以没影响主机的权重，也就没有迁移了

# 删除节点
ceph osd rm osd.28
# 这个是从集群里面删除这个节点的记录

# 删除节点认证（不删除编号会占住）
ceph auth del osd.28

from __future__ import print_function
from nose import SkipTest
from nose.tools import eq_ as eq, ok_ as ok, assert_raises
from rados import (Rados, Error, RadosStateError, Object, ObjectExists,
                   ObjectNotFound, ObjectBusy, requires, opt,
                   LIBRADOS_ALL_NSPACES, WriteOpCtx, ReadOpCtx,
                   LIBRADOS_SNAP_HEAD, LIBRADOS_OPERATION_BALANCE_READS, LIBRADOS_OPERATION_SKIPRWLOCKS, MonitorLog)
import time
import threading
import json
import errno
import os
import re
import sys

# Are we running Python 2.x
_python2 = sys.version_info[0] < 3

rados = Rados(conffile='')
rados.conf_parse_env('FOO_DOES_NOT_EXIST_BLAHBLAH')
rados.conf_parse_env()
rados.connect()
print(rados.list_pools())
print(rados.pool_exists('test_pool'))
ioctx = rados.open_ioctx('test_pool')
print(ioctx.get_last_version())

ioctx.write('abc', b'abc')
print(ioctx.read('abc'))
stats = ioctx.get_stats()
print(stats)

ioctx.write_full('abc', b'abc')
ioctx.append('abc', b'b')
ioctx.append('abc', b'c')  #追加
ioctx.write('abc', b'a\0b\0c')
print(ioctx.read('abc'))
ioctx.trunc('abc', 2)  #对象大小设置为2字节
stats = ioctx.get_stats()  #获取pool属性
print(stats)
size=ioctx.stat('abc')[0] #对象大小
print(size)

object_names = [obj.key for obj in ioctx.list_objects()]   #列出所有对象
print(object_names, ioctx.list_objects())

ioctx.set_namespace("ns1")      #设置命名空间
ioctx.set_xattr('abc', "tttabc", b"111")  #设置pool扩展属性
print( ioctx.get_stats())
print(ioctx.get_xattr('abc', "tttabc"))
for key, value in ioctx.get_xattrs('abc'): #获取pool所有扩展属性
    print(key, value)

print(ioctx.stat('abc'))  #对象属性
obj = list(ioctx.list_objects())[0] #对象扩展属性
for key, value in obj.get_xattrs():
    print(key, value )
    
ioctx.create_snap('foo')  #创建快照
listed_snaps = [snap.name for snap in ioctx.list_snaps()]  #查看快照
print(ioctx.list_snaps(),listed_snaps)
snap = ioctx.lookup_snap('foo')  #查询快照
print(snap.name,snap.get_timestamp())
ioctx.write("insnap", b"contents1")
ioctx.create_snap("snap1")
ioctx.remove_object("insnap")
ioctx.snap_rollback("insnap", "snap1")
print(ioctx.read("insnap"))
ioctx.remove_snap("snap1")
ioctx.remove_object("insnap")

ioctx.remove_snap('foo') 

#omap,这里有一个ceph的原则，就是所有存储的不管是块设备、对象存储、文件存储最后都转化成了底层的对象object，
# 这个object包含3个元素data，xattr，omap。data是保存对象的数据，xattr是保存对象的扩展属性，每个对象文件都可以设置文件的属性，
# 这个属性是一个key/value值对，但是受到文件系统的限制，key/value对的个数和每个value的大小都进行了限制。
# 如果要设置的对象的key/value不能存储在文件的扩展属性中，还存在另外一种方式保存omap，
# omap实际上是保存到了key/vaule  值对的数据库levelDB中，在这里value的值限制要比xattr中好的多。
#l略

with WriteOpCtx() as write_op:
    write_op.new(0)
    ioctx.operate_write_op(write_op, "write_ops")
    print(ioctx.read('write_ops'))
    write_op.write_full(b'1')
    write_op.append(b'2')
    ioctx.operate_write_op(write_op, "write_ops")
    print(ioctx.read('write_ops'))
    write_op.write_full(b'12345')
    write_op.write(b'x', 2)  #位置2写
    ioctx.operate_write_op(write_op, "write_ops")
    print(ioctx.read('write_ops'))
    write_op.write_full(b'12345')
    write_op.zero(2, 2)
    ioctx.operate_write_op(write_op, "write_ops")
    print(ioctx.read('write_ops'))
    write_op.write_full(b'12345')
    write_op.truncate(2)  #对象大小缩短为2个
    print(list(ioctx.list_objects()))
    #write_op.remove()

ioctx.write("foo", b"") # ensure object exists
ret, buf = ioctx.execute("foo", "hello", "say_hello", b"hfjsdh")
print(ret, buf )

cmd = {"prefix":"osd dump", "format":"json"}
ret, buf, errs = rados.mon_command(json.dumps(cmd), b'') #执行mon命令,
release = json.loads(buf.decode("utf-8")).get("require_osd_release",None)  #ceph版本
print(ret, buf, errs,release)
print(ioctx.application_list())

object1 = Object(ioctx, 'abc')
print(object1.read(3))


#rados.delete_pool('test_pool')
#rados.shutdown()


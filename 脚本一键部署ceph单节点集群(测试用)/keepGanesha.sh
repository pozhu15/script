crontab -r  #非首次执行取消这个注释，会删除crontab之前的用户设置，防止重复添加
#cron进程中加入守护ganesha的脚本，使其每10秒执行一次
crontab -l > ~/tmpcrontab123.conf
cat>~/tmpcrontab123.conf<<EOF
* * * * * /etc/cron_gs/check_gs_ps.sh
* * * * * sleep 10; /etc/cron_gs/check_gs_ps.sh
* * * * * sleep 20; /etc/cron_gs/check_gs_ps.sh
* * * * * sleep 30; /etc/cron_gs/check_gs_ps.sh
* * * * * sleep 40; /etc/cron_gs/check_gs_ps.sh
* * * * * sleep 50; /etc/cron_gs/check_gs_ps.sh
EOF
crontab ~/tmpcrontab123.conf	#配置加载入cron
rm ~/tmpcrontab123.conf
mkdir /etc/cron_gs
rm /etc/cron_gs/check_gs_ps.sh
cat>/etc/cron_gs/check_gs_ps.sh<<EOF
ps -fe|grep ganesha.nfsd|grep -v grep	#检测ganesha进程是否启动
if [ \$? -eq 0 ]
then
	echo \`date\` activing> /var/log/check_ganesha.txt
	#mount -a 
else		#没启动择执行下面的脚本进行启动ganesha
    echo \`date\` restart> /var/log/check_ganesha.txt
    ganesha.nfsd -f /etc/ganesha/ganesha.conf -L /var/log/nfs-ganesha.log -N NIV_DEBUG  #启动ganesha
    mount -a    #执行挂载
fi
EOF
chmod 777 /etc/cron_gs/check_gs_ps.sh
#重启cron
rm -rf /var/run/crond.pid
crond reload
crond restart
#read -p "输入挂载命令" Protocols
#cat /etc/fstab>~/tmpfstab123.conf
#echo $Protocols>~/tmpfstab123.conf
#mv ~/tmpfstab123.conf /etc/fstab

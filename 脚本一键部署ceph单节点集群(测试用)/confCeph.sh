 #关闭selinux和iptables
 systemctl stop firewalld
 systemctl disable firewalld
 setenforce 0
 sed -i '/SELINUX/s/enforcing/disabled/' /etc/selinux/config
 setenforce 0
 #设置时间同步
 yum -y install ntp
systemctl enable ntpd
systemctl start ntpd
ntpdate -u cn.pool.ntp.org
hwclock --systohc
timedatectl set-timezone Asia/Shanghai
#
hostnamectl set-hostname  node1
#检测添加hosts
str=`grep -o -E "^[0-9]+.+node0" /etc/hosts`
if [ x"$str" = x ];then 
	IP=`ifconfig |xargs -n 8 |grep -o -E 'ens.+netmask'|grep -o -E '[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+'`
	echo "$IP node0">>/etc/hosts
fi
# 设置ceph的源，否则有些组件无法下载
cat >/etc/yum.repos.d/ceph.repo<< eof
[ceph]	#路径为以上的x86_64目录
name=ceph
baseurl=https://mirrors.aliyun.com/ceph/rpm-15.2.2/el7/x86_64/
gpgcheck=0
enabled=1

[ceph-noarch]	#路径为以上noarch目录
name=ceph-noarch
baseurl=https://mirrors.aliyun.com/ceph/rpm-15.2.2/el7/noarch/
gpgcheck=0
enabled=1
eof
yum install ceph-deploy -y
ssh-keygen
cd /etc/ceph
ceph-deploy new  node1
echo "osd_pool_default_size = 1" >> ceph.conf
echo "mon_allow_pool_delete = true" >> ceph.conf
ceph-deploy  mon  create-initial
mkfs.xfs /dev/sda3       #格式化分区
ceph-deploy osd create --data /dev/sda3 node1
ceph-deploy admin node1
ceph-deploy mgr create  node1
#部署rgw
ceph-deploy rgw create node1
#创建RGW用户
#radosgw-admin user create --uid=t1 --display-name=t1
#radosgw-admin user info --uid t1
radosgw-admin user create --uid=admin --display-name=admin --access_key=admin --secret=admin
cat >/etc/ganesha/ganesha.conf<<eof
NFSv4
{
    grace_period=10;
} 
EXPORT {
	Anonymous_Uid = 0;
	Anonymous_Gid = 0;
	grace_period = 10;
	Export_ID = 1;
	Path = "/";
	Pseudo = "/";
	Access_Type = RW;
	Protocols = 3,4;
	Transports = TCP,UDP;
	Squash = root;
	FSAL {
	    secret_access_key = "admin";
        user_id = "admin";
        name = "RGW";
        access_key_id = "admin";
    }
}
RGW
{
        ceph_conf = "/etc/ceph/ceph.conf";
        name = "client.rgw.node0";
        cluster = "ceph";
        init_args = "--log_file=/var/log/rgw.log --debug-rgw=20 --rgw_nfs_s3_fast_attrs=true";
}

eof
mkdir /mnt/test
#设置挂载路径
str=`grep -o -E '/mnt/test.+nfs.+defaults.+vers=' /etc/fstab`
if [ x"$str" = x ];then 
	echo "node0:/  /mnt/test nfs4 defaults,sync,vers=4.1 0 0">>/etc/fstab
fi
chmod 755 keepGanesha.sh
./keepGanesha.sh	#添加守护进程

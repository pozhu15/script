#include <iostream>
#include "Python.h"
using namespace std;
 
// 简易版本
void method1()
{
    Py_Initialize();
    PyRun_SimpleString("import sys");
    PyRun_SimpleString("sys.path.append('./')");
    PyRun_SimpleString("import pytest");
    PyRun_SimpleString("pytest.printHelloWold()");
    Py_Finalize();
}
 
// 初级版本
void method2()
{
    Py_Initialize();
    PyRun_SimpleString("import sys");
    PyRun_SimpleString("sys.path.append('./')");
    PyObject *pModule = NULL;
    PyObject *pFunc = NULL;
    pModule = PyImport_ImportModule("pytest");                 //Python文件名
    pFunc = PyObject_GetAttrString(pModule, "printHelloWold"); //Python文件中的函数名
    PyEval_CallObject(pFunc, NULL);                            //调用函数,NULL表示参数为空
    Py_Finalize();                                             //Py_Finalize,和Py_Initialize相对应的.
}
 
// 带参数和返回值版本
void method3()
{
    Py_Initialize();
    PyRun_SimpleString("import sys");
    PyRun_SimpleString("sys.path.append('./')");
    PyObject *pModule = NULL;
    PyObject *pFunc = NULL;
    pModule = PyImport_ImportModule("pytest");      //Python文件名
    pFunc = PyObject_GetAttrString(pModule, "add"); //Python文件中的函数名
    //创建参数:
    PyObject *pArgs = PyTuple_New(2);                 //函数调用的参数传递均是以元组的形式打包的,2表示参数个数
    PyTuple_SetItem(pArgs, 0, Py_BuildValue("i", 6)); //0--序号,i表示创建int型变量
    PyTuple_SetItem(pArgs, 1, Py_BuildValue("i", 8)); //1--序号
    //返回值
    PyObject *pReturn = NULL;
    pReturn = PyEval_CallObject(pFunc, pArgs); //调用函数
    //将返回值转换为int类型
    int result;
    PyArg_Parse(pReturn, "i", &result); //i表示转换成int型变量
    cout << "6 + 8 = " << result << endl;
    Py_Finalize();
}
 
int main()
{
    // 简易版本
    method1();
 
    // 初级版本
    method2();
    
    // 带参数和返回值版本
    method3();
    
    return 0;
}

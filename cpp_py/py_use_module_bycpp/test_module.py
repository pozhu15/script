
import ceph_module  # noqa

import os

class MgrModule(ceph_module.BaseMgrModule):

    def __init__(self, module_name, py_modules_ptr, this_ptr):
        self.module_name = module_name

        super(MgrModule, self).__init__(py_modules_ptr, this_ptr)

    def __del__(self):
        pass

 
    @property
  

    def get(self, data_name):
        return self._ceph_get(data_name)

    def get_server(self, hostname):
        return self._ceph_get_server(hostname)

class _ModuleProxy(object):
    def __init__(self):
        self._mgr = None

    def init(self, module_inst):
        global logger
        self._mgr = module_inst
        logger._logger = self._mgr._logger

    def __getattr__(self, item):
        if self._mgr is None:
            raise AttributeError("global manager module instance not initialized")
        return getattr(self._mgr, item)

mgr = _ModuleProxy()


def add(a, b):
    
    try:
        import os #ceph_module 
    except :
        exit(0) #print('err')
        
    #m=mgr.get_server(10)
    c = a+b #+int(mgr.get_server(10))
    return c

  
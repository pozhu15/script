#include <iostream>
#include <map>
#include <sstream>
#include "Python.h"
using namespace std;

#include "BaseMgrModule.h"

#define PY_MAJOR_VERSION 3


std::string get_site_packages()
{
  std::stringstream site_packages;

  // CPython doesn't auto-add site-packages dirs to sys.path for us,
  // but it does provide a module that we can ask for them.
  auto site_module = PyImport_ImportModule("site");
  //ceph_assert(site_module);

  auto site_packages_fn = PyObject_GetAttrString(site_module, "getsitepackages");
  if (site_packages_fn != nullptr) {
    auto site_packages_list = PyObject_CallObject(site_packages_fn, nullptr);
    //ceph_assert(site_packages_list);

    auto n = PyList_Size(site_packages_list);
    for (Py_ssize_t i = 0; i < n; ++i) {
      if (i != 0) {
        site_packages << ":";
      }
      site_packages << PyString_AsString(PyList_GetItem(site_packages_list, i));
    }

    Py_DECREF(site_packages_list);
    Py_DECREF(site_packages_fn);
  } else {
    // Fall back to generating our own site-packages paths by imitating
    // what the standard site.py does.  This is annoying but it lets us
    // run inside virtualenvs :-/

    auto site_packages_fn = PyObject_GetAttrString(site_module, "addsitepackages");
    //ceph_assert(site_packages_fn);

    auto known_paths = PySet_New(nullptr);
    auto pArgs = PyTuple_Pack(1, known_paths);
    PyObject_CallObject(site_packages_fn, pArgs);
    Py_DECREF(pArgs);
    Py_DECREF(known_paths);
    Py_DECREF(site_packages_fn);

    auto sys_module = PyImport_ImportModule("sys");
    //ceph_assert(sys_module);
    auto sys_path = PyObject_GetAttrString(sys_module, "path");
    //ceph_assert(sys_path);

    dout(1) << "sys.path:" << dendl;
    auto n = PyList_Size(sys_path);
    bool first = true;
    for (Py_ssize_t i = 0; i < n; ++i) {
      dout(1) << "  " << PyString_AsString(PyList_GetItem(sys_path, i)) << dendl;
      if (first) {
        first = false;
      } else {
        site_packages << ":";
      }
      site_packages << PyString_AsString(PyList_GetItem(sys_path, i));
    }

    Py_DECREF(sys_path);
    Py_DECREF(sys_module);
  }

  Py_DECREF(site_module);

  return site_packages.str();
}



#if PY_MAJOR_VERSION >= 3
PyObject* init_ceph_module()
#else
void init_ceph_module()
#endif
{
  static PyMethodDef module_methods[] = {
    {nullptr, nullptr, 0, nullptr}
  };
#if PY_MAJOR_VERSION >= 3
  static PyModuleDef ceph_module_def = {
    PyModuleDef_HEAD_INIT,
    "ceph_module",
    nullptr,
    -1,
    module_methods,
    nullptr,
    nullptr,
    nullptr,
    nullptr
  };
  PyObject *ceph_module = PyModule_Create(&ceph_module_def);
#else
  PyObject *ceph_module = Py_InitModule("ceph_module", module_methods);
#endif
  //ceph_assert(ceph_module != nullptr);
  map<const char*, PyTypeObject*> classes{
    {{"BaseMgrModule", &BaseMgrModuleType}
    }
  };
  for (auto [name, type] : classes) {
    type->tp_new = PyType_GenericNew;
    if (PyType_Ready(type) < 0) {
      //ceph_abort();
    }
    Py_INCREF(type);

    PyModule_AddObject(ceph_module, name, (PyObject *)type);
  }
#if PY_MAJOR_VERSION >= 3
  return ceph_module;
#endif
}


void method3()
{
    Py_Initialize();
    PyRun_SimpleString("import sys");
    PyRun_SimpleString("sys.path.append('./')");
    PyObject *pModule = NULL;
    PyObject *pFunc = NULL;
    pModule = PyImport_ImportModule("test_module");      //Python文件名
    pFunc = PyObject_GetAttrString(pModule, "add"); //Python文件中的函数名
    //创建参数:
    PyObject *pArgs = PyTuple_New(2);                 //函数调用的参数传递均是以元组的形式打包的,2表示参数个数
    PyTuple_SetItem(pArgs, 0, Py_BuildValue("i", 6)); //0--序号,i表示创建int型变量
    PyTuple_SetItem(pArgs, 1, Py_BuildValue("i", 8)); //1--序号
    //返回值
    PyObject *pReturn = NULL;
    pReturn = PyEval_CallObject(pFunc, pArgs); //调用函数
    //将返回值转换为int类型
    int result;
    PyArg_Parse(pReturn, "i", &result); //i表示转换成int型变量
    cout << "6 + 8 = " << result << endl;
    Py_Finalize();
}

int main()
{
  #if PY_MAJOR_VERSION >= 3
#define WCHAR(s) L ## #s
  Py_SetProgramName(const_cast<wchar_t*>(WCHAR("testpy")));
#undef WCHAR
#else
  Py_SetProgramName(const_cast<char*>("testpy"));
#endif

    PyImport_AppendInittab("ceph_module", init_ceph_module);
    Py_InitializeEx(0);
    
    /*if (! PyEval_ThreadsInitialized()) {
      PyEval_InitThreads();
    }
    PyEval_SaveThread();
*/

    PyRun_SimpleString("import sys");
    PyRun_SimpleString("sys.path.append('./')");
    PyObject *pModule = NULL;
    PyObject *pFunc = NULL;
    pModule = PyImport_ImportModule("test_module");    //Python文件名
    pFunc = PyObject_GetAttrString(pModule, "add"); //Python文件中的函数名
    //创建参数:
    PyObject *pArgs = PyTuple_New(2);                 //函数调用的参数传递均是以元组的形式打包的,2表示参数个数
    PyTuple_SetItem(pArgs, 0, Py_BuildValue("i", 6)); //0--序号,i表示创建int型变量
    PyTuple_SetItem(pArgs, 1, Py_BuildValue("i", 8)); //1--序号
    //返回值
    PyObject *pReturn = NULL;
    pReturn = PyEval_CallObject(pFunc, pArgs); //调用函数
    //将返回值转换为int类型
    int result;
    PyArg_Parse(pReturn, "i", &result); //i表示转换成int型变量
    cout << "6 + 8 = " << result << endl;


    Py_Finalize();

    //method3();
    return 0;
}
// -*- mode:C++; tab-width:8; c-basic-offset:2; indent-tabs-mode:t -*-
// vim: ts=8 sw=2 smarttab
/*
 * Ceph - scalable distributed file system
 *
 * Copyright (C) 2016 John Spray <john.spray@redhat.com>
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1, as published by the Free Software
 * Foundation.  See file COPYING.
 */

/**
 * The interface we present to python code that runs within
 * ceph-mgr.  This is implemented as a Python class from which
 * all modules must inherit -- access to the Ceph state is then
 * available as methods on that object.
 */

#include "Python.h"


#include "BaseMgrModule.h"
//#include "PyFormatter.h"
#include <algorithm>


#define PLACEHOLDER ""


typedef struct {
  PyObject_HEAD
  char *py_modules;
  char *this_module;
} BaseMgrModule;



static PyObject*
ceph_state_get(BaseMgrModule *self, PyObject *args)
{
  int num =0;
  if (!PyArg_ParseTuple(args, "s:ceph_state_get", &num)) {
    return NULL;
  }
  
  /*
  PyFormatter f(false, true);
  f.open_object_section("server");

  f->dump_string("hostname", 'hostname');
  f->open_array_section("services");
  f->open_object_section("service");
  f->dump_string("type", 'str_type');
  f->dump_string("id", 'svc_name');
  f->close_section();
  f->close_section();
  f->dump_string("ceph_version", 'ceph_version');

  f.close_section();


  return f.get();
  */
  return PyLong_FromLong(num);
}


static PyObject*
ceph_get_server(BaseMgrModule *self, PyObject *args)
{
  int num =0;
  if (!PyArg_ParseTuple(args, "z:ceph_get_server", &num)) {
    return NULL;
  }
  return PyLong_FromLong(num);

}



PyMethodDef BaseMgrModule_methods[] = {
  {"_ceph_get", (PyCFunction)ceph_state_get, METH_VARARGS,
   "Get a cluster object"},

  {"_ceph_get_server", (PyCFunction)ceph_get_server, METH_VARARGS,
   "Get a server object"},

  {NULL, NULL, 0, NULL}
};


static PyObject *
BaseMgrModule_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    BaseMgrModule *self;

    self = (BaseMgrModule *)type->tp_alloc(type, 0);

    return (PyObject *)self;
}

static int
BaseMgrModule_init(BaseMgrModule *self, PyObject *args, PyObject *kwds)
{
    PyObject *py_modules_capsule = nullptr;
    PyObject *this_module_capsule = nullptr;
    static const char *kwlist[] = {"py_modules", "this_module", NULL};

    if (! PyArg_ParseTupleAndKeywords(args, kwds, "OO",
                                      const_cast<char**>(kwlist),
                                      &py_modules_capsule,
                                      &this_module_capsule)) {
        return -1;
    }

    self->py_modules = static_cast<char*>(PyCapsule_GetPointer(
        py_modules_capsule, nullptr));
    //ceph_assert(self->py_modules);
    self->this_module = static_cast<char*>(PyCapsule_GetPointer(
        this_module_capsule, nullptr));
    //ceph_assert(self->this_module);

    return 0;
}

PyTypeObject BaseMgrModuleType = {
  PyVarObject_HEAD_INIT(NULL, 0)
  "ceph_module.BaseMgrModule", /* tp_name */
  sizeof(BaseMgrModule),     /* tp_basicsize */
  0,                         /* tp_itemsize */
  0,                         /* tp_dealloc */
  0,                         /* tp_print */
  0,                         /* tp_getattr */
  0,                         /* tp_setattr */
  0,                         /* tp_compare */
  0,                         /* tp_repr */
  0,                         /* tp_as_number */
  0,                         /* tp_as_sequence */
  0,                         /* tp_as_mapping */
  0,                         /* tp_hash */
  0,                         /* tp_call */
  0,                         /* tp_str */
  0,                         /* tp_getattro */
  0,                         /* tp_setattro */
  0,                         /* tp_as_buffer */
  Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,        /* tp_flags */
  "ceph-mgr Python Plugin", /* tp_doc */
  0,                         /* tp_traverse */
  0,                         /* tp_clear */
  0,                         /* tp_richcompare */
  0,                         /* tp_weaklistoffset */
  0,                         /* tp_iter */
  0,                         /* tp_iternext */
  BaseMgrModule_methods,     /* tp_methods */
  0,                         /* tp_members */
  0,                         /* tp_getset */
  0,                         /* tp_base */
  0,                         /* tp_dict */
  0,                         /* tp_descr_get */
  0,                         /* tp_descr_set */
  0,                         /* tp_dictoffset */
  (initproc)BaseMgrModule_init,                         /* tp_init */
  0,                         /* tp_alloc */
  BaseMgrModule_new,     /* tp_new */
};


from distutils.core import setup, Extension
module1 = Extension('Test', sources = ['cpp_module.cpp'])
setup (name = 'Test',
       version = '1.0',
       description = 'This is a demo package',
       ext_modules = [module1])
name=py_mgr_whl
version=17.5.0
rgw_name=$name-$version

current_path=$(cd "$(dirname $0)";pwd)
cd $current_path
buildroot=$current_path/buildroot
#rpmbuild工具检测安装
yum list installed | grep rpm-devel
if [ $? -ne 0 ];then
	yum install rpm-devel.x86_64 -y
	yum install rpmdevtools -y
	rpmdev-setuptree
fi
rpath_check=`grep "^#.*%__arch_install_post" ~/.rpmmacros`
if [ x"$rpath_check" = x  ];then 
	echo "监测到没有关闭check-rpaths，将会使打包失败，请手动编辑文件：vi ~/.rpmmacros  "
	echo "#%__arch_install_post /usr/lib/rpm/check-rpaths /usr/lib/rpm/check-buildroot         #注释掉这行"
	read -p "注释完成按任意键继续" str1
fi

source /opt/rh/devtoolset-8/enable

rm -rf ~/rpmbuild/BUILDROOT/*
rm -rf ~/rpmbuild/BUILD/*
mkdir $rgw_name
cp mgr_wheel.spec $rgw_name
tar -czf $rgw_name.tar.gz $rgw_name
cp $rgw_name.tar.gz ~/rpmbuild/SOURCES
cp mgr_wheel.spec  ~/rpmbuild/SPECS/
rpmbuild -ba ~/rpmbuild/SPECS/mgr_wheel.spec ||exit
echo "rpm包已生成在~/rpmbuild/RPMS路径下，请用以下命令安装对应的rpm包"
echo "安装前关闭ganesha进程，安装完再启动"
echo "rpm -ivh ~/rpmbuild/RPMS/x86_64/librgw2-*.rpm --nodeps --force --replacefiles"



%bcond_with make_check
%bcond_without ceph_test_package
%ifarch s390 s390x
%bcond_with tcmalloc
%else
%bcond_without tcmalloc
%endif
%if 0%{?fedora} || 0%{?rhel}
%bcond_without selinux
%bcond_without cephfs_java
%bcond_without lttng
%bcond_without libradosstriper
%bcond_without ocf
%bcond_without amqp_endpoint
%global _remote_tarball_prefix https://download.ceph.com/tarballs/
%endif
%if 0%{?suse_version}
%bcond_with selinux
%bcond_with cephfs_java
%bcond_with amqp_endpoint
#Compat macro for new _fillupdir macro introduced in Nov 2017
%if ! %{defined _fillupdir}
%global _fillupdir /var/adm/fillup-templates
%endif
%if 0%{?is_opensuse}
%bcond_without libradosstriper
%bcond_without ocf
%else
%bcond_with libradosstriper
%bcond_with ocf
%endif
%ifarch x86_64 aarch64 ppc64le
%bcond_without lttng
%else
%bcond_with lttng
%endif
%endif
%bcond_with seastar
%if 0%{?fedora} >= 29 || 0%{?suse_version} >= 1500 || 0%{?rhel} >= 8
# distros that need a py3 Ceph build
%bcond_with python2
%else
# distros that need a py2 Ceph build
%bcond_without python2
%endif
%if 0%{?fedora} || 0%{?suse_version} >= 1500
# distros that ship cmd2 and/or colorama
%bcond_without cephfs_shell
%else
# distros that do _not_ ship cmd2/colorama
%bcond_with cephfs_shell
%endif
%if 0%{without python2}
%global _defined_if_python2_absent 1
%endif

%if %{with selinux}
# get selinux policy version
%{!?_selinux_policy_version: %global _selinux_policy_version 0.0.0}
%endif

%{!?_udevrulesdir: %global _udevrulesdir /lib/udev/rules.d}
%{!?tmpfiles_create: %global tmpfiles_create systemd-tmpfiles --create}
%{!?python3_pkgversion: %global python3_pkgversion 3}
%{!?python3_version_nodots: %global python3_version_nodots 3}
%{!?python3_version: %global python3_version 3}
# define _python_buildid macro which will expand to the empty string when
# building with python2
%global _python_buildid %{?_defined_if_python2_absent:%{python3_pkgversion}}

# unify libexec for all targets
%global _libexecdir %{_exec_prefix}/lib

# disable dwz which compresses the debuginfo
%global _find_debuginfo_dwz_opts %{nil}

#################################################################################
# main package definition
#################################################################################
Name:		py_mgr_whl
Version:	1.7.2
Release:	3%{?dist}
%if 0%{?fedora} || 0%{?rhel}
Epoch:		2
%endif

# define _epoch_prefix macro which will expand to the empty string if epoch is
# undefined
%global _epoch_prefix %{?epoch:%{epoch}:}

Summary:	User space components of the Ceph file system
License:	LGPL-2.1 and CC-BY-SA-3.0 and GPL-2.0 and BSL-1.0 and BSD-3-Clause and MIT
%if 0%{?suse_version}
Group:		System/Filesystems
%endif
Source0:	%{?_remote_tarball_prefix}py_mgr_whl-1.7.2.tar.gz
%if 0%{?suse_version}
# _insert_obs_source_lines_here
ExclusiveArch:  x86_64 aarch64 ppc64le s390x
%endif
#################################################################################
# dependencies that apply across all distro families
#################################################################################
Requires(post):	binutils
%if 0%{with selinux}
BuildRequires:	checkpolicy
BuildRequires:	selinux-policy-devel
%endif
BuildRequires:	gperf

BuildRequires:	cryptsetup
BuildRequires:	fuse-devel
%if 0%{?rhel} == 7
# devtoolset offers newer make and valgrind-devel, but the old ones are good
# enough.
%ifarch x86_64
BuildRequires:	devtoolset-8-gcc-c++ >= 8.2.1
%else
BuildRequires:	devtoolset-7-gcc-c++ >= 7.3.1-5.13
%endif
%else
BuildRequires:	gcc-c++
%endif
BuildRequires:	gdbm
%if 0%{with tcmalloc}
%if 0%{?fedora} || 0%{?rhel}
BuildRequires:	gperftools-devel >= 2.6.1
%endif
%if 0%{?suse_version}
BuildRequires:	gperftools-devel >= 2.4
%endif
%endif
BuildRequires:	leveldb-devel > 1.2
BuildRequires:	libaio-devel
BuildRequires:	libblkid-devel >= 2.17
BuildRequires:	libcurl-devel
BuildRequires:	libcap-ng-devel
BuildRequires:	libudev-devel
BuildRequires:	libnl3-devel
BuildRequires:	liboath-devel
BuildRequires:	libtool
BuildRequires:	libxml2-devel
BuildRequires:	make
BuildRequires:	ncurses-devel
BuildRequires:	parted
BuildRequires:	patch
BuildRequires:	perl
BuildRequires:	pkgconfig
BuildRequires:  procps
BuildRequires:	snappy-devel
BuildRequires:	sudo
BuildRequires:	udev
BuildRequires:	util-linux
BuildRequires:	valgrind-devel
BuildRequires:	which
BuildRequires:	xfsprogs
BuildRequires:	xfsprogs-devel
BuildRequires:	xmlstarlet
BuildRequires:	yasm
%if 0%{with amqp_endpoint}
BuildRequires:  librabbitmq-devel
%endif
%if 0%{with make_check}
BuildRequires:  jq
BuildRequires:	libuuid-devel
BuildRequires:	python%{_python_buildid}-bcrypt
BuildRequires:	python%{_python_buildid}-coverage
BuildRequires:	python%{_python_buildid}-nose
BuildRequires:	python%{_python_buildid}-pecan
BuildRequires:	python%{_python_buildid}-requests
BuildRequires:	python%{_python_buildid}-six
BuildRequires:	python%{_python_buildid}-tox
BuildRequires:	python%{_python_buildid}-virtualenv
%if 0%{?rhel} == 7
BuildRequires:  pyOpenSSL%{_python_buildid}
%else
BuildRequires:  python%{_python_buildid}-pyOpenSSL
%endif
BuildRequires:	socat
%endif
%if 0%{with seastar}
BuildRequires:  c-ares-devel
BuildRequires:  gnutls-devel
BuildRequires:  hwloc-devel
BuildRequires:  libpciaccess-devel
BuildRequires:  lksctp-tools-devel
BuildRequires:  protobuf-devel
BuildRequires:  ragel
BuildRequires:  systemtap-sdt-devel
BuildRequires:  yaml-cpp-devel
%endif
#################################################################################
# distro-conditional dependencies
#################################################################################
%if 0%{?suse_version}
BuildRequires:  pkgconfig(systemd)
BuildRequires:	systemd-rpm-macros
%{?systemd_requires}
PreReq:		%fillup_prereq
BuildRequires:	fdupes
BuildRequires:	net-tools
BuildRequires:	libbz2-devel
BuildRequires:	mozilla-nss-devel
BuildRequires:	keyutils-devel
BuildRequires:  libopenssl-devel
BuildRequires:  lsb-release
BuildRequires:  openldap2-devel
#BuildRequires:  krb5
#BuildRequires:  krb5-devel
BuildRequires:  cunit-devel
BuildRequires:	python%{_python_buildid}-setuptools
BuildRequires:	python%{_python_buildid}-Cython
BuildRequires:	python%{_python_buildid}-PrettyTable
BuildRequires:	python%{_python_buildid}-Sphinx
BuildRequires:  rdma-core-devel
BuildRequires:	liblz4-devel >= 1.7
# for prometheus-alerts
BuildRequires:  golang-github-prometheus-prometheus
%endif
%if 0%{?fedora} || 0%{?rhel}
Requires:	systemd
BuildRequires:  boost-random
BuildRequires:	nss-devel
BuildRequires:	keyutils-libs-devel
BuildRequires:	libibverbs-devel
BuildRequires:  librdmacm-devel
BuildRequires:  openldap-devel
#BuildRequires:  krb5-devel
BuildRequires:  openssl-devel
BuildRequires:  CUnit-devel
BuildRequires:  redhat-lsb-core
%if 0%{with python2}
BuildRequires:	python2-Cython
%endif

BuildRequires:	python%{_python_buildid}-prettytable
BuildRequires:	python%{_python_buildid}-sphinx
BuildRequires:	lz4-devel >= 1.7
%endif
# distro-conditional make check dependencies
%if 0%{with make_check}
%if 0%{?fedora} || 0%{?rhel}
BuildRequires:	python%{_python_buildid}-cherrypy
BuildRequires:	python%{_python_buildid}-jwt
BuildRequires:	python%{_python_buildid}-routes
BuildRequires:	python%{_python_buildid}-werkzeug
BuildRequires:  xmlsec1
%endif
%if 0%{?suse_version}
BuildRequires:	python%{_python_buildid}-CherryPy
BuildRequires:	python%{_python_buildid}-PyJWT
BuildRequires:	python%{_python_buildid}-Routes
BuildRequires:	python%{_python_buildid}-Werkzeug
BuildRequires:	python%{_python_buildid}-numpy-devel
BuildRequires:  xmlsec1-devel
%endif
%endif
# lttng and babeltrace for rbd-replay-prep
%if %{with lttng}
%if 0%{?fedora} || 0%{?rhel}
BuildRequires:	lttng-ust-devel
BuildRequires:	libbabeltrace-devel
%endif
%if 0%{?suse_version}
BuildRequires:	lttng-ust-devel
BuildRequires:  babeltrace-devel
%endif
%endif
%if 0%{?suse_version}
BuildRequires:	libexpat-devel
%endif
%if 0%{?rhel} || 0%{?fedora}
BuildRequires:	expat-devel
%endif
#hardened-cc1
%if 0%{?fedora} || 0%{?rhel}
BuildRequires:  redhat-rpm-config
%endif
%if 0%{with seastar}
%if 0%{?fedora} || 0%{?rhel}
BuildRequires:  cryptopp-devel
BuildRequires:  numactl-devel
BuildRequires:  protobuf-compiler
%endif
%if 0%{?suse_version}
BuildRequires:  libcryptopp-devel
BuildRequires:  libnuma-devel
%endif
%endif

%description
Ceph is a massively scalable, open-source, distributed storage system that runs
on commodity hardware and delivers object, block and file system storage.


#################################################################################
# subpackages
#################################################################################

%package -n python2-cryptography
Summary:	library
%if 0%{?suse_version}
Group:		System/Libraries
%endif
%description -n python2-cryptography
This package provides a library implementation of the RADOS gateway
(distributed object store with S3 and Swift personalities).

%package -n pyOpenSSL
Summary:	library
%if 0%{?suse_version}
Group:		System/Libraries
%endif
%description -n pyOpenSSL
This package provides a library implementation of the RADOS gateway
(distributed object store with S3 and Swift personalities).

%package -n python-six
Summary:	library
%if 0%{?suse_version}
Group:		System/Libraries
%endif
%description -n python-six
This package provides a library implementation of the RADOS gateway
(distributed object store with S3 and Swift personalities).

%package -n python-cherrypy
Summary:	library
%if 0%{?suse_version}
Group:		System/Libraries
%endif
%description -n python-cherrypy
This package provides a library implementation of the RADOS gateway
(distributed object store with S3 and Swift personalities).


%package -n python2-mgr-need
Summary:	library
%if 0%{?suse_version}
Group:		System/Libraries
%endif
%description -n python2-mgr-need
This package provides a library implementation of the RADOS gateway
(distributed object store with S3 and Swift personalities).

#################################################################################
# common
#################################################################################
%prep
%setup -q 

%build
# LTO can be enabled as soon as the following GCC bug is fixed:
# https://gcc.gnu.org/bugzilla/show_bug.cgi?id=48200
%define _lto_cflags %{nil}

python -m pip install CherryPy==3.2.2 --target=usr/lib64/python2.7/site-packages/
python -m pip install CherryPy==3.2.2 --target=usr/lib/python2.7/site-packages/

python -m pip install pyOpenSSL==17.5.0 --target=usr/lib64/python2.7/site-packages/
python -m pip install pyOpenSSL==17.5.0 --target=usr/lib/python2.7/site-packages/

python -m pip install cryptography==3.3.2 --target=usr/lib64/python2.7/site-packages/
python -m pip install cryptography==3.3.2 --target=usr/lib/python2.7/site-packages/

python -m pip install bcrypt==3.1.7 --target=usr/lib64/python2.7/site-packages/
python -m pip install bcrypt==3.1.7 --target=usr/lib/python2.7/site-packages/

%if 0%{with make_check}
%check
# run in-tree unittests
#cd build
#ctest "$CEPH_MFLAGS_JOBS"
%endif


%install
%define _unpackaged_files_terminate_build 0 

cp -r * %{buildroot}
%define _unpackaged_files_terminate_build 0
%if 0%{?suse_version}
# create __pycache__ directories and their contents
%py3_compile %{buildroot}%{python3_sitelib}
# prometheus alerts

# hardlink duplicate files under /usr to save space
%fdupes %{buildroot}%{_prefix}
%endif

%if 0%{?rhel} == 8
%py_byte_compile %{__python3} %{buildroot}%{python3_sitelib}
%endif

%clean
#rm -rf %{buildroot}

#################################################################################
# files and systemd scriptlets
#################################################################################
%files


%files -n python2-cryptography
%{_libdir}/python2.7/site-packages/cryptography
%{_libdir}/python2.7/site-packages/cryptography-3.3.2.dist-info

/usr/lib/python2.7/site-packages/cryptography
/usr/lib/python2.7/site-packages/cryptography-3.3.2.dist-info


%files -n python-six
%{_libdir}/python2.7/site-packages/six.py
%{_libdir}/python2.7/site-packages/six-1.16.0.dist-info

/usr/lib/python2.7/site-packages/six.py
/usr/lib/python2.7/site-packages/six-1.16.0.dist-info


%files -n python-cherrypy
%{_libdir}/python2.7/site-packages/cherrypy
%attr(755,root,root) %{_libdir}/python2.7/site-packages/bin/cherryd
%{_libdir}/python2.7/site-packages/CherryPy-3.2.2.dist-info

/usr/lib/python2.7/site-packages/cherrypy
%attr(755,root,root) /usr/lib/python2.7/site-packages/bin/cherryd
/usr/lib/python2.7/site-packages/CherryPy-3.2.2.dist-info


%files -n pyOpenSSL
%{_libdir}/python2.7/site-packages/OpenSSL
%{_libdir}/python2.7/site-packages/pyOpenSSL-17.5.0.dist-info

/usr/lib/python2.7/site-packages/OpenSSL
/usr/lib/python2.7/site-packages/pyOpenSSL-17.5.0.dist-info




%files -n python2-mgr-need
%{_libdir}/python2.7/site-packages/cffi
%{_libdir}/python2.7/site-packages/cffi-1.14.5.dist-info
%{_libdir}/python2.7/site-packages/cffi.libs
%{_libdir}/python2.7/site-packages/enum
%{_libdir}/python2.7/site-packages/enum34-1.1.10.dist-info
%{_libdir}/python2.7/site-packages/ipaddress-1.0.23.dist-info
%{_libdir}/python2.7/site-packages/pycparser 
%{_libdir}/python2.7/site-packages/pycparser-2.20.dist-info 
%{_libdir}/python2.7/site-packages/bcrypt 
%{_libdir}/python2.7/site-packages/bcrypt-3.1.7.dist-info 
%{_libdir}/python2.7/site-packages/ipaddress.py

%attr(755,root,root) %{_libdir}/python2.7/site-packages/_cffi_backend.so


/usr/lib/python2.7/site-packages/cffi
/usr/lib/python2.7/site-packages/cffi-1.14.5.dist-info
/usr/lib/python2.7/site-packages/cffi.libs
/usr/lib/python2.7/site-packages/enum
/usr/lib/python2.7/site-packages/enum34-1.1.10.dist-info
/usr/lib/python2.7/site-packages/ipaddress-1.0.23.dist-info
/usr/lib/python2.7/site-packages/pycparser 
/usr/lib/python2.7/site-packages/pycparser-2.20.dist-info 
/usr/lib/python2.7/site-packages/bcrypt 
/usr/lib/python2.7/site-packages/bcrypt-3.1.7.dist-info 
/usr/lib/python2.7/site-packages/ipaddress.py

%attr(755,root,root) /usr/lib/python2.7/site-packages/_cffi_backend.so

%post -n python2-mgr-need

%postun 

%changelog

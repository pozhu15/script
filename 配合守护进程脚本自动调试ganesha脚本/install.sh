if [ $1 -eq 1 ]
then
	cd $(dirname $0); pwd -P
	cd src/
	rm -rf build
	mkdir build
	cd build
	cmake -DUSE_FSAL_RGW=ON -DUSE_FSAL_CEPH=ON ../
	cmake ../  
	make|| exit
	make install
fi

umount -f /mnt/cephNfs4.2 -f /mnt/cephNfs4.1 -f /mnt/cephNfs3 -f /mnt/cephNfs4

ID=`ps -ef | grep "ganesha.nfsd" | grep -v "grep" | awk '{print $2}'`
kill -9 $ID

echo "已就绪:"

ps -ef |grep ganesha.nfsd
mount |grep mnt

echo "等待重启"

while true
	do
		ps -fe|grep ganesha.nfsd|grep -v grep
      if [ $? -eq 0 ]
			then
				break
			else		
				sleep 1
			fi
    done 

echo "已重启"
sleep 1
ps -ef |grep ganesha
mount |grep mnt
echo "开始实验"
rm  /var/log/nfs_pro.log
echo "这是写入内容" >/mnt/cephNfs3/test/io111.txt &
tail -f /var/log/nfs_pro.log
#cat /var/log/nfs_pro.log

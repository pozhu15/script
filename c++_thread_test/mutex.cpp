#include <iostream>
#include <thread>
#include <unistd.h>  
#include<mutex>
using namespace std;

class st{
    public:
    st(int n):i(n) {
        cout<<"  start"<<endl;
    }
    int i {1};
    ~st(){
         cout<<"  end"<<endl;
    }
};

auto i {1};

int cnt = 20;
mutex m;
void t1()
{
    while (cnt > 0)
    {
        m.lock();
        if (cnt > 0)
        {
            --cnt;
            cout << cnt << endl;
        }
        m.unlock();
    }
}
void t2()
{
    while (cnt > 0)
    {
        m.lock();
        if (cnt > 0)
        {
            --cnt;
            cout << cnt << endl;
        }
        m.unlock();
    }
}
void test3()
{
    cout << "t33333\n";
}
int main()
{
    st s {0};
    thread th1(t1);  //实例化一个线程对象th1，使用函数t1构造，然后该线程就开始执行了（t1()）
   // thread th2(t2,123); //向线程调用的函数传递参数，只需要在构造thread的实例时，依次传入即可
    thread th2(t2); 

    th1.join(); //join让主线程等待直到该子线程执行结束
    test3();    //等待th1执行完成才执行
    sleep(1);
    th2.join();
  
    /*
    th1.detach();  //detach将线程和main主线程分离，main不用等子线程完成就结束
    test3();    //等待th1执行完成才执行
    sleep(1);
    th2.detach();
    */

    test3();

    cout << "here is main\n\n";

    return 0;
}








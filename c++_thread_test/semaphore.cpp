#include<semaphore.h>
#include<iostream>
#include<thread>
#include <unistd.h>  
using namespace std;

/*
//semaphore相关函数:
// 销毁
int sem_destroy(sem_t *sem); 
// 
int sem_wait(sem_t *sem);  // 资源减少1
int sem_trywait(sem_t *sem);  
int sem_post(sem_t *sem);  // 资源增加1
int sem_getvalue(sem_t *sem); 
*/

// g++ -lpthread pthread_create_semaphore.cpp

sem_t binSem;

void* helloWorld(void* arg);

int main() {
     // Result for System call
    int res = 0;

     // Initialize semaphore
     res = sem_init(&binSem, 0, 0);
    if (res) {
         printf("Semaphore initialization failed!!\n");
         exit(EXIT_FAILURE);
     }

     // Create thread
     pthread_t thdHelloWorld;
// 第一个参数是要绑定的线程对象
// 第二个参数是可以被用来设置线程属性，一般使用NULL
// 第三个参数需要绑定的函数名称
// 第四个参数是函数传入的参数
     res = pthread_create(&thdHelloWorld, NULL, helloWorld, NULL);
    if (res) {
         printf("Thread creation failed!!\n");
         exit(EXIT_FAILURE);
     }

    while(1) {
         // Post semaphore
         sem_post(&binSem);
         printf("In main, sleep several seconds.\n");
        sleep(1);
     }

     // Wait for thread synchronization
     void *threadResult;
     res = pthread_join(thdHelloWorld, &threadResult);
    if (res) {
         printf("Thread join failed!!\n");
         exit(EXIT_FAILURE);
     }

     exit(EXIT_SUCCESS);
}

void* helloWorld(void* arg) {
    while(1) {
         // Wait semaphore
         sem_wait(&binSem);
         printf("Hello World\n");
     }
}

import re,sys
spec_file=sys.argv[1]
spec=''
with open(spec_file,'r') as sp:
    spec=sp.read()
#print(spec)
pattern = re.compile(r'%package base.+(?=%package mgr\s)',re.S) 
pattern1 = re.compile(r'%package fuse.+(?=%prep\s)',re.S) 
pattern2 = re.compile(r'%files base.+(?=%files mgr\s)',re.S) 
pattern3 = re.compile(r'%files mon.+(?=%changelog\s)',re.S) 
pattern4 = re.compile(r'\s(?=make "\$CEPH_MFLAGS_JOBS"\s)',re.S) 
pattern5 = re.compile(r'(?<=%install)',re.S) 
pattern6 = re.compile(r'pushd build',re.S) 
pattern7 = re.compile(r'chmod.+sample\.ceph\.conf') 
pattern8 = re.compile(r'(?<=make "\$CEPH_MFLAGS_JOBS")\s') 
pattern9 = re.compile(r'(?<=popd)\s') 
content = pattern.sub('', spec)
content = pattern1.sub('', content)
content = pattern2.sub('', content)
content = pattern3.sub('', content)
content = pattern4.sub('cd src/mgr\n', content)
content = pattern5.sub('\n%define _unpackaged_files_terminate_build 0 \n', content)
content = pattern6.sub('pushd build/src/mgr', content)
content = pattern7.sub('', content)
content = pattern8.sub('\ncd ../pybind/mgr\nmake "$CEPH_MFLAGS_JOBS"\ncd ../../../systemd\nmake "$CEPH_MFLAGS_JOBS"', content)
content = pattern9.sub('\npushd build/src/pybind/mgr\nmake DESTDIR=%{buildroot} install\npopd\n\npushd build/systemd\nmake DESTDIR=%{buildroot} install\npopd\n', content)
match=pattern9.findall(spec)
print(match)
with open(spec_file,'w') as sp:
    sp.write(content)



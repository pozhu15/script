#!/bin/bash
echo -e "\033[3;32m --- 开始执行Lakestor的rpm包生成脚本 ---\033[0m"
set -e

#scl enable devtoolset-8 bash

# 获取输入参数
source_path=$1
if [[ $source_path = "" ]];then
    echo -e "\033[3;33m使用当前目录下的工程Lakestor...\033[0m"
    source_path=`pwd`'/Lakestor'
fi

echo -e "\033[3;33m当前工程Moses的路径为${source_path}\033[0m"
if [ ! -d "$source_path" ];then
    echo -e "\033[3;31m错误：指定目录${source_path}下工程Lakestor不存在！...\033[0m"
	  exit
fi

echo -e "\033[3;36m请输入version号：\033[0m"
read version
if [[ $version = "" ]];then
    echo -e "\033[3;31m错误！version不能为空！\033[0m"
	  exit
fi
version=v${version}

echo -e "\033[3;36m请输入release号：\033[0m"
read release
if [[ $release = "" ]];then
    echo -e "\033[3;31m错误！release不能为空！\033[0m"
	  exit
fi

# 复制Lakestor副本到当前目录并命名为 Lakestor_${version}
echo -e "\033[3;36m开始拷贝Lakestor...\033[0m"
cp $source_path -r Lakestor_${version}

echo -e "\033[3;36m开始处理Lakestor的ceph.spec文件...\033[0m"
# 替换version信息
sed -i "s/ceph-14.2.7/Lakestor_$version/g" Lakestor_${version}/ceph.spec
sed -i "s/14.2.7/Lakestor_$version/g" Lakestor_${version}/ceph.spec
sed -i "s/Lakestor_v[0-9]*.[0-9]*.[0-9]*.[0-9]+/Lakestor_$version/g" Lakestor_${version}/ceph.spec

# 替换release信息
sed -i "s/[0-9]*%{?dist}$/${release}%{?dist}/g" Lakestor_${version}/ceph.spec

# 替换 make-dist中的版本信息
sed -i "/git describe --match/d" Lakestor_${version}/make-dist
sed -i "/^version=/c version=Lakestor_$version" Lakestor_${version}/make-dist

echo -e "\033[3;36m开始清理rpmbuild目录...\033[0m"
rm -rf ~/rpmbuild/BUILD/*
rm -rf ~/rpmbuild/SPECS/*
rm -rf ~/rpmbuild/SOURCES/*
rm -rf ~/rpmbuild/RPMS/*
rpmdev-setuptree

echo -e "\033[3;36m是否只打包mgr？y/n：\033[0m"
read release
if [[ $release = "y" ]];then
    python mgr-pack.py Lakestor_${version}/ceph.spec
fi

echo -e "\033[3;36m开始填充rpmbuild目录...\033[0m"
cp Lakestor_${version}/ceph.spec ~/rpmbuild/SPECS/
tar -cvf Lakestor_${version}.tar.bz2 Lakestor_$version
rm -rf Lakestor_${version}
mv Lakestor_${version}.tar.bz2 ~/rpmbuild/SOURCES/

source /opt/rh/devtoolset-8/enable 
echo -e "\033[3;36m开始生成rpm包...\033[0m"
rpmbuild -ba ~/rpmbuild/SPECS/ceph.spec

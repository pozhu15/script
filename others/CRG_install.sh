
cephversion='15.2.7' 
current_path=$(cd "$(dirname $0)";pwd)

current_time=`date +"%Y-%m-%d %H:%M:%S"`
echo $current_time ---------start--------- >$current_path/CRG_install.log
	
runWithLog(){
	current_time=`date +"%Y-%m-%d %H:%M:%S"`
	echo "$current_time $2 $1 ">>$current_path/CRG_install.log
	$1 ||exit
}

function printhelp(){
	case $1 in
		"-d") 
			echo "下载可选参数：cmake，ceph，userspace-rcu，ganesha，all"
		;;
		"-m") 
			echo "编译可选参数：cmake，ceph(附加参数 -om，仅make)，userspace-rcu，ganesha"
		;;
		"-i") 
			echo "编译安装可选参数：cmake，ceph(附加参数 -om)，userspace-rcu，ganesha"
		;;
		"-p") 
			echo "打包rpm包可选参数：rgw，ceph"
		;;
		*)  
			echo 'init 	编译环境的初始化(安装相关依赖和组件)'
			echo '-d 		download选项'
			echo '-m 		仅编译选项'
			echo '-i 		编译安装选项'
			echo '-p 		打包rpm包选项'
			echo 'start 	一键安装部署ceph，ganesha环境'
			echo 'showlist  查看已安装软件列表及测试ceph'
		;;
	esac
}
function downloadobj(){	#下载源码
	cd $current_path; 
	yum list installed | grep git
	if [ $? -eq 0 ];then
		runWithLog "yum install -y git "
	fi
	case $1 in
		ceph) 
			#wget http://download.ceph.com/tarballs/ceph-$cephversion.tar.gz
			#tar -xzvf ceph-$cephversion.tar.gz
			git clone http://10.20.5.5:10080/ehualu/Lakestor.git
		;;
		userspace-rcu) git clone git://git.liburcu.org/userspace-rcu.git
		;;
		ganesha) git clone -b V3.3 https://github.com/nfs-ganesha/nfs-ganesha.git --recursive 
		;;
		cmake) 
			wget https://cmake.org/files/v3.5/cmake-3.5.1.tar.gz
			tar -xzvf cmake-3.5.1.tar.gz
		;;
		all)  
			downloadobj "cmake"
			downloadobj "ceph"
			downloadobj "userspace-rcu"
			downloadobj "ganesha"
		;;
		*)  printhelp -d
		;;
	esac
	cd $current_path;  
}
function buildobj(){  #编译
	cd $current_path; 
	case $1 in
		ceph) chmod -R 755 Lakestor
			cd Lakestor 
			centosVer=`lsb_release -a |grep -o -E 'Release:\s[1-9]+\.' |grep -o -E '[1-9]+'`
			shellName=`echo $SHELL |grep -o -E '/[a-z]+$' |grep -o -E '[a-z]+$'`
			#scl enable devtoolset-$centosVer $shellName
			runWithLog "source /opt/rh/devtoolset-8/enable "
			if [ $# -eq 1 ]||[ $2 != "-om" ];then
				#./do_cmake.sh
				if [ $# -eq 2 ]&&[ $2 == "-cm" ];then
					cd src/pybind/mgr/dashboard/frontend/ 
					npm cache clean --force
					npm config set registry http://registry.npm.taobao.org # 更改npm镜像服务器地址
					runWithLog "rm -rf node_modules/ " # 若有该目录，则删除
					cd -
				else 
					rm -rf build
					runWithLog "./do_cmake.sh "
				fi
			fi
			gccv= gcc -v
			echo "$gccv">>$current_path/CRG_install.log
			cd build
			runWithLog "make "
			
		;;
		userspace-rcu) chmod -R 755 userspace-rcu
			cd userspace-rcu
			runWithLog "./bootstrap  "
			runWithLog "./configure  "
			runWithLog "make "
		;;
		ganesha) cd nfs-ganesha
			cd src/
			rm -rf build
			mkdir build
			cd build
			runWithLog "cmake -DUSE_FSAL_RGW=ON -DUSE_FSAL_CEPH=ON ../ "
			runWithLog "cmake ../  "
			runWithLog "make "
		;;
		cmake) 
			cd cmake-3.5.1
			runWithLog "./bootstrap  "
			runWithLog "gmake "
		;;
		*)  printhelp -m
		;;
	esac
	cd $current_path;  
}
function buildInstallObj(){	#编译安装
	cd $current_path; 
	case $1 in
		ceph) buildobj ceph $2
			cd Lakestor/build
			runWithLog "make install "
		;;
		userspace-rcu) runWithLog "buildobj userspace-rcu" ""
			cd userspace-rcu 
			runWithLog "make install " 
		;;
		ganesha) runWithLog "buildobj ganesha" ""
			cd nfs-ganesha/src/build
			runWithLog "make install "
		;;
		cmake) runWithLog "buildobj cmake" ""
			cd cmake-3.5.1
			runWithLog "gmake install  "
		;;
		*)  printhelp -i
		;;
	esac
	cd $current_path; 
}
packRgw(){	#打包rgw rpm
	cd $current_path; 
	if [ $1 -eq 0 ];then  #通过编译方式打包
		name=rgw
		version=14.2.0
		rgw_name=$name-$version
		rm -rf $rgw_name
		mkdir $rgw_name
		mkdir $rgw_name/lib
		cur_path=$(cd "$(dirname $0)";pwd)

		cp Lakestor/build/lib/librgw.so.2.0.0 $rgw_name/lib
		cp Lakestor/build/lib/librgw.so.2 $rgw_name/lib
		cp Lakestor/build/lib/librgw.so $rgw_name/lib
		cp Lakestor/build/lib/librgw_admin_user.so.1.0.0 $rgw_name/lib
		cp Lakestor/build/lib/librgw_admin_user.so.0 $rgw_name/lib
		cp Lakestor/build/lib/librgw_admin_user.so $rgw_name/lib
		strip $rgw_name/lib/*
		tar -czf $rgw_name.tar.gz $rgw_name
		yum list installed | grep rpm-devel
		if [ $? -ne 0 ];then
			yum install rpm-devel.x86_64 -y
			yum install rpmdevtools -y
			rpmdev-setuptree
		fi
		#vi ~/.rpmmacros
		# #%__arch_install_post /usr/lib/rpm/check-rpaths /usr/lib/rpm/check-buildroot
		cp $rgw_name.tar.gz ~/rpmbuild/SOURCES
cat > packrgw.spec <<eof
Name:		rgw
Version:    	14.2.0
Release:	1%{?dist}
Summary:	rgw service 
Group:		Applications/System
License:        gk-license

URL:		None
Source0:	rgw-14.2.0.tar.gz

%description
This package is the rgw service  daemon.

%prep
rm -rf %_topdir/BUILD
rm -rf %_topdir/RPMS
rm -rf %_topdir/BUILDROOT
mkdir %_topdir/BUILD
mkdir %_topdir/RPMS
mkdir %_topdir/BUILDROOT
%setup -q 

%install
mkdir -p %{buildroot}%{_libdir}
cp -r lib/* %{buildroot}%{_libdir}

%define _unpackaged_files_terminate_build 0
%clean
rm -rf %{buildroot}
%pre

%post -p /sbin/ldconfig

%preun -p /sbin/ldconfig

%postun

%files
%{_libdir}/librgw.so.*
%{_libdir}/librgw_admin_user.so.*
%{_libdir}/librgw.so
%{_libdir}/librgw_admin_user.so
%clean                        
rm -rf %{buildroot}
eof
		rpmbuild -ba packrgw.spec
		echo "rpm包已生成在~/rpmbuild/RPMS路径下，请用以下命令安装对应的rpm包"
		echo "rpm -ivh ~/rpmbuild/RPMS/$rgw_name*.rpm --nodeps --force"
	else	#原始的spec文件打包
		yum list installed | grep rpm-devel
		if [ $? -ne 0 ];then
			yum install rpm-devel.x86_64 -y
			yum install rpmdevtools -y
			rpmdev-setuptree
		fi
		name=ceph
		version=14.2.7
		ceph_name=$name-$version
		rm -rf $ceph_name
		runWithLog "cp -r Lakestor $ceph_name"
		rm -rf $ceph_name/build
		#cp specfiles/ceph.spec $ceph_name
		#cp specfiles/CMakeLists.txt $ceph_name
		#cp specfiles/make-dist $ceph_name
		cd $current_path; 
		tar -cjf $ceph_name.tar.bz2 $ceph_name
		rm  ~/rpmbuild/SPECS/ceph.spec
		rm  ~/rpmbuild/SOURCES/$ceph_name.tar.bz2
		cp specfiles/ceph.spec  ~/rpmbuild/SPECS/ceph.spec
		cp $ceph_name.tar.bz2 ~/rpmbuild/SOURCES/$ceph_name.tar.bz2
		rpmbuild -ba ~/rpmbuild/SPECS/ceph.spec
		#rpm -ivh rpms/$rgw_name* --nodeps --force
		echo "rpm包已生成在~/rpmbuild/RPMS路径下，请用以下命令安装对应的rpm包"
		echo "rpm -ivh ~/rpmbuild/RPMS/librgw2_tmp*.rpm --nodeps --force"

	fi
	
}
function packRgwToRpm(){	#rgw打包为rpm包
	cd $current_path; 
	case $1 in
		rgw) 
			runWithLog "packRgw 1	"
		;;
		ceph) 
			echo " no	"
		;;
		*)  printhelp -p
		;;
	esac
	cd $current_path; 
}
function initenv(){ #初始化编译环境
	runWithLog "yum clean all"
   	wget -O /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-7.repo
	wget -O /etc/yum.repos.d/epel.repo http://mirrors.aliyun.com/repo/epel-7.repo
   	runWithLog "yum makecache"
	runWithLog "yum update -y"
   	cd $(dirname $0); 
   runWithLog "yum install redhat-lsb -y"
	runWithLog "sudo yum install centos-release-scl -y"

	runWithLog "yum install -y gcc  "
	runWithLog "yum install -y git "

	mkdir ~/.pip
cat > ~/.pip/pip.conf << eof
[global]
timeout = 6000
index-url = https://pypi.tuna.tsinghua.edu.cn/simple
eof

	centosVer=`lsb_release -a |grep -o -E 'Release:\s[1-9]+\.' |grep -o -E '[1-9]+'`
	runWithLog "sudo yum install -y devtoolset-8 "
	shellName=`echo $SHELL |grep -o -E '/[a-z]+$' |grep -o -E '[a-z]+$'`
	bashrcfile=~/.$shellName"rc"
cat >> $bashrcfile << eof	
export PYTHONPATH=/usr/local/lib64/python2.7/site-packages:$PYTHONPATH  
eof
	source $bashrcfile
	#runWithLog "scl enable devtoolset-$centosVer $shellName"
	source /opt/rh/devtoolset-8/enable 
	gccv= gcc -v
	echo "$gccv">>$current_path/CRG_install.log
	cd $current_path; 
	chmod -R 755 Lakestor 
	runWithLog "cd Lakestor"
	#git init
	runWithLog "./install-deps.sh "

	runWithLog "yum install -y autoconf "
	runWithLog "yum install -y libtool"
	runWithLog "yum install -y bison flex"
	runWithLog "yum install -y doxygen"
	runWithLog "yum install -y openssl-devel"
	runWithLog "yum install -y gcc-c++"
	runWithLog "yum install -y krb5-libs"
	runWithLog "yum install -y krb5-devel"
	runWithLog "yum install -y libuuid-devel"
	runWithLog "yum install -y nfs-utils"
	runWithLog "yum install -y epel-release"

	
cat >/etc/yum.repos.d/ceph.repo<< eof
[ceph]	
name=ceph
baseurl=https://mirrors.aliyun.com/ceph/rpm-15.2.2/el7/x86_64/
gpgcheck=0
enabled=1
[ceph-noarch]	
name=ceph-noarch
baseurl=https://mirrors.aliyun.com/ceph/rpm-15.2.2/el7/noarch/
gpgcheck=0
enabled=1
eof

	runWithLog "yum install -y librgw2-devel"
	runWithLog "yum install -y libcephfs-devel"
	
	runWithLog "buildInstallObj cmake " "编译安装cmake"
	runWithLog "buildInstallObj userspace-rcu"
	str=`grep "^/usr/local/lib$" /etc/ld.so.conf`
	if [ x"$str" = x ];then 
cat >> /etc/ld.so.conf << eof
/usr/local/lib
eof
	/sbin/ldconfig -v
	fi

	cd ~
	curl --silent --location https://rpm.nodesource.com/setup_10.x | $shellName -
	runWithLog "yum install nodejs -y"
	
	cd $current_path; 
}

function onekey(){	#一键部署安装
	#runWithLog "downloadobj all" "下载所有源代码"
	runWithLog "initenv" "初始化编译环境"
 	runWithLog "buildInstallObj ceph" "安装ceph"
 	runWithLog "buildInstallObj ganesha" "安装nfs-ganesha"
	echo "完成！"
	showpkglist
}
showpkglist(){ #查看已安装软件列表
	echo "----已安装："
	cmake --version
	ceph -v
	ganesha.nfsd -v
	echo "----测试ceph："
	cd $current_path/Lakestor/build
	make vstart
	../src/vstart.sh --debug --new -x --localhost --bluestore
	./bin/ceph -s
	../src/stop.sh # shut down 测试集群
}

case $1 in
	init) initenv  
	;;
	start) onekey  
	;;
	"-d") downloadobj $2 $3   
	;;
	"-m") buildobj $2 $3 
	;;
	"-i") buildInstallObj $2 $3 
	;;
	"-p") packRgwToRpm $2 $3 
	;;
	showlist) showpkglist  
	;;
	*) printhelp
	;;
esac



